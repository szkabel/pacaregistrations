function maxImgVal = getMaxImgValFromClass(currImg)

switch class(currImg)
    case 'uint8'
        maxImgVal = 255;
    case 'uint16'
        maxImgVal = 65535;
    otherwise
        maxImgVal = 1;
end

end

