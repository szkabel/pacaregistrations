% {
[imgMUC_prep,imgMUC_sat] = preprocessForRoughAlign(imgMUC);
[imgSMA_prep,imgSMA_sat] = preprocessForRoughAlign(imgSMA);
figure; imshow(imgMUC_prep);
figure; imshow(imgSMA_prep);
% }
disp('Preprocessing is done');
pause(2);
disp('Registration...');
roughTrans = roughAlign(imgMUC_prep,imgSMA_prep);

disp('Warping...');
imgSMA_prep_RoughRegistered = imwarp(imgSMA_prep,roughTrans,'OutputView',imref2d(size(imgMUC_prep)));
figure; imshowpair(imgMUC_prep,imgSMA_prep_RoughRegistered);

roughTransBig = roughTrans;
roughTransBig.T(3,[1 2]) = (size(imgMUC_sat)./size(imgMUC_prep)).*roughTransBig.T(3,[1 2]);
imgSMA_sat_RoughRegistered = imwarp(imgSMA_sat,roughTransBig,'OutputView',imref2d(size(imgMUC_sat)));
figure; imshowpair(imgMUC_sat,imgSMA_sat_RoughRegistered);

%disp('Smoother elastic registration...')
%MUC_sat_Hist = imhist(imgMUC_sat);
%imgSMA_sat_RoughRegistered_equalized = histeq(imgSMA_sat_RoughRegistered,MUC_sat_Hist);
%imgSMA_sat_RoughRegistered_GPU = gpuArray(imgSMA_sat_RoughRegistered);
%imgMUC_sat_GPU = gpuArray(imgMUC_sat);
%[D,imgSMA_sat_elastic_GPU] = imregdemons(imgSMA_sat_RoughRegistered_GPU,imgMUC_sat_GPU,[256 128 64],'AccumulatedFieldSmoothing',1.5,'DisplayWaitbar',true);
%imgSMA_sat_elastic = gather(imgSMA_sat_elastic_GPU);

%[D,imgSMA_sat_elastic] = imregdemons(imgSMA_sat_RoughRegistered_equalized,imgMUC_sat,[256 128 64],'AccumulatedFieldSmoothing',2,'DisplayWaitbar',true);
%[1024 512 256]

%figure; imshowpair(imgMUC_sat,imgSMA_sat_elastic);
