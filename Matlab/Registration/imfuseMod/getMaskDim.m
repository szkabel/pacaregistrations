function [desiredSize,xmax,xmaxi,xmini,ymax,ymini,ymaxi] = getMaskDim(allMasks)

xSum = sum(allMasks,2);
ySum = sum(allMasks,1);
xmax = max(xSum);
xmaxi = find(ySum,1,'last');
xmini = find(ySum,1,'first');    
ymax = max(ySum);
ymaxi = find(xSum,1,'last');
ymini = find(xSum,1,'first');    
desiredSize = [ ymax xmax ]; % THIS SIZE IN MATLAB SIZES!

end

