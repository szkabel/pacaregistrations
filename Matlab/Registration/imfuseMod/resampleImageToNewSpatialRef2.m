function [B,BMask,RB] = resampleImageToNewSpatialRef2(A,RA,RB,interpMethod,resType)
%resampleImageToNewSpatialRef Resample spatially referenced image to new grid.
%
%   [B,RB,MASK] = resampleImageToNewSpatialRef(A,RA,RB,FILLVALUE,METHOD)
%   resamples the spatially referenced image A,RA to a new grid whose world
%   extent and resolution is defined by RB. During resampling, the output
%   image B is assigned the value fillValue. METHOD defines the
%   interpolation method used during resampling, valid values are
%   'nearest','bilinear', and 'bicubic'. The output B,RB is an output
%   spatially referenced image. MASK is a logical image which is false
%   where fillValues were assigned and true where values of A were used to
%   define B.

% Copyright 2012 The MathWorks, Inc.

% Modified by Abel Szkalisity 2020.03.19.?
% resType refers to 'Padded' or 'MaskOnly' depending on what is required

%[bIntrinsicX,bIntrinsicY] = meshgrid(1:RB.ImageSize(2),1:RB.ImageSize(1));

%[xWorldOverlayLoc,yWorldOverlayLoc] = RB.intrinsicToWorld(bIntrinsicX,bIntrinsicY);

[xIntrinsicOfAinB,yIntrinsicOfAinB] = RB.worldToIntrinsic(RA.XWorldLimits,RA.YWorldLimits); %#ok<ASGLU> used in eval

if nargin<5, resType = 'Padded'; end

letters = {'x','X';'y','Y'};
relations = {'<','ceil';'>','floor'};
xlims = [0 0];
ylims = [0 0];

for i=1:size(letters,1)
    for j=1:2
           eval( [ letters{i,1} 'lims(' num2str(j) ') = round(' letters{i,1} 'IntrinsicOfAinB(' num2str(j) '));']);
        if eval( [ letters{i,1} 'lims(' num2str(j) ') ' relations{j,1} ' RB.' letters{i,2} 'IntrinsicLimits(' num2str(j) ')'])
            eval([ letters{i,1} 'lims(' num2str(j) ') = ' relations{j,2} '( RB.' letters{i,2} 'IntrinsicLimits(' num2str(j) ') );']);
        else                
            eval([ letters{i,1} 'lims(' num2str(j) ') = ' relations{j,2} '( ' letters{i,1} 'lims(' num2str(j) ') );']);
        end       
    end
end

validCoordinateMask = false(RB.ImageSize);
validCoordinateMask( ylims(1):ylims(2),xlims(1):xlims(2) ) = true;

%xIntrinsicLocFilt = round(xIntrinsicLoc(validCoordinateMask));
%yIntrinsicLocFilt = round(yIntrinsicLoc(validCoordinateMask));
nofImgLayers = size(A,3);
if strcmp(resType,'Padded')
    B = zeros([size(validCoordinateMask) nofImgLayers]);
end

% Resample to form output image
%if isa(A,'double')
%    BFilt = images.internal.interp2d(A,xIntrinsicLocFilt,yIntrinsicLocFilt,interpMethod,fillValue);
%else
%    BFilt = images.internal.interp2d(single(A),single(xIntrinsicLocFilt),single(yIntrinsicLocFilt),...
%            interpMethod,fillValue,true);
%end
desiredSize = getMaskDim(validCoordinateMask);
if ~all(size(A) == desiredSize)
    BFilt = imresize(A,desiredSize,interpMethod);
else
    BFilt = A;
end

BMask = repmat(validCoordinateMask,[1 1 nofImgLayers]);
if strcmp(resType,'Padded')
    B(BMask) = BFilt;
elseif strcmp(resType,'MaskOnly')
    B = BFilt;
end

% Move B back to original type
B = cast(B,class(A));

