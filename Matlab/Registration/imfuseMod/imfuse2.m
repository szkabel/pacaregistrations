function [C,RC] = imfuse2(varargin)
%IMFUSE Composite of two images.
%   C = IMFUSE(A,B) creates a composite image of two images A and B. If A
%   and B are different sizes, the smaller dimensions are padded with zeros
%   such that the two images are the same size before computing the
%   composite. The output, C, is a numeric matrix containing a fused
%   version of images A and B.
%
%   [C,RC] = IMFUSE(A,RA,B,RB) creates a fused image C from the images A
%   and B using the spatial referencing information provided in RA and RB.
%   RA, RB, and RC are spatial referencing objects of class imref2d. The
%   output RC defines the spatial referencing information for the output
%   fused image C.
%
%   C = IMFUSE(___,METHOD) creates a fused image C from the images A
%   and B using the algorithm specified by METHOD.  Values of METHOD can
%   be:
%
%      'falsecolor'   : Create a composite RGB image showing A and B overlayed
%                       in different color bands. This is the default.
%      'blend'        : Overlay A and B using alpha blending.
%      'checkerboard' : Create image with alternating rectangular regions 
%                       from A and B.
%      'diff'         : Difference image created from A and B.
%      'montage'      : Put A and B next to each other in the same image.
%
%   IMFUSE(___,PARAM1,VAL1,PARAM2,VAL2,...) display the images,
%   specifying parameters and corresponding values that control various
%   aspects of display and blending.  Parameter name case does not matter.
%
%   Parameters include:
%
%   'Scaling'       Both images are converted to a common data type before
%                   being displayed.  Additional scaling of image data is
%                   controled by the 'Scaling' parameter of IMSHOWPAIR.
%
%                   Valid 'Scaling' values are:
%                      'independent' :  Images are scaled independently from
%                                       each other.
%                      'joint'       :  Images are scaled as a single data set.
%                                       This option can be useful when
%                                       visualizing registrations of
%                                       monomodal images in which one image
%                                       has a large amount of fill values
%                                       outside the dynamic range of your
%                                       images.
%                      'none'        :  No additional scaling.
%
%                                       Default value: 'independent'
%
%   'ColorChannels' Assign each image to specific color channels in the
%                   output image.  This method can only be used with the
%                   'falsecolor' method.
%
%                   Valid 'ColorChannels' values are:
%                      [R G B]        :  A three element vector that
%                                         specifies which image to assign
%                                         to the red, green, and blue
%                                         channels.  The R, G, and B values
%                                         must be 1 (for the first input
%                                         image), 2 (for the second input
%                                         image), and 0 (for neither image).
%                      'red-cyan'      :  A shortcut for the vector [1 2 2],
%                                         which is suitable for red/cyan
%                                         stereo anaglyphs.
%                      'green-magenta' :  A shortcut for the vector [2 1 2],
%                                         which is a high contrast option
%                                         ideal for people with many kinds
%                                         of color blindness.
%
%                                         Default value: 'green-magenta'
%
%   Class Support
%   -------------
%   A and B are numeric arrays. The output C is a numeric array of class
%   uint8. RA, RB, and RC are spatial referencing objects of class imref2d.
%
%   Tips
%   ----
%   Use imfuse to create composite visualizations that you can save to a
%   file.  Use imshowpair to display composite visualizations to the
%   screen.
%
%   Examples
%   --------
%   % Two images with a rotation offset
%   A = imread('cameraman.tif');
%   B = imrotate(A,5,'bicubic','crop');
%   C = imfuse(A,B,'blend','Scaling','joint');
%   % Create a PNG file containing blended overlay image
%   imwrite(C,'my_blend_overlay.png');
%
%   % Use red for one image, green for the other, and yellow for areas of
%   % similar intensity between the two images.
%   A = imread('cameraman.tif');
%   B = imrotate(A,5,'bicubic','crop');
%   C = imfuse(A,B,'falsecolor','Scaling','joint','ColorChannels',[1 2 0]);
%   % Create a PNG file containing the red/green/yellow image
%   imwrite(C,'my_blend_red-green.png');
%
%   % Create fused image of two spatially referenced images.
%   A = dicomread('knee1.dcm');
%   RA = imref2d(size(A));
%   B = imresize(A,2);
%   RB = imref2d(size(B));
%   RB.XWorldLimits = RA.XWorldLimits
%   RB.YWorldLimits = RA.YWorldLimits
%   % Images align even though B is twice the size of A because
%   % RA and RB define A and B as having the same extent in the world
%   % coordinate system
%   [C,RC] = imfuse(A,RA,B,RB);
%
%   See also IMREF2D, IMREGISTER, IMSHOWPAIR, IMTRANSFORM.

%   Copyright 2011-2018 The MathWorks, Inc.
%
%   Modified by Abel Szkalisity 2020.03.17. to facilitate proper blending
%   by bwdist AND to handle more than 2 images for fusing!
%   Note: only implemented for Blending properly yet!

% basic input validation
[Imgs,Refs,method,options] = parse_inputs(varargin);

% pull P/V pairs out of options struct
scaling = options.Scaling;

imgIsRGB = cellfun(@(c) size(c,3) > 1,Imgs);

RC = calcUnionRefSpace(Refs);
nofLayersInBigImg = size(Imgs{1},3);

if any(imgIsRGB), C = zeros([RC.ImageSize nofLayersInBigImg]); else, C = zeros(RC.ImageSize); end

regionMap = identifyRegions(Refs);
%load('200320_RegionMaps','regionMap');

regKeys = regionMap.keys;

%Do some preprocessing for fastening:
compNums = zeros(length(regKeys),1);
for i=1:length(regKeys)
    currReg = regionMap(regKeys{i});
    if ~isempty(currReg.polygon.Vertices)
        compNums(i) = length(currReg.composition);
    else
        compNums(i) = 0;
    end
end
[~,sortIdx] = sort(compNums);
reordRegKeys = regKeys(sortIdx);
reordRegKeys(compNums(sortIdx) == 0) = [];

for i=1:length(reordRegKeys)
    fprintf('Stitching regions... [%d/%d]\n',i,length(reordRegKeys));
    currReg = regionMap(reordRegKeys{i});
    [currImgs,currMasks,fullRegRef] = calculateOverlayImages(Imgs(currReg.composition),Refs(currReg.composition));
    
    if length(currImgs)>1
        switch method
            case 'blend'
                [regionFuse,regionFuseRef] = local_createBlend(currImgs,currMasks,fullRegRef,imgIsRGB(currReg.composition),scaling);
        end
    else
        regionFuse = currImgs{1};
        regionFuseRef = Refs{currReg.composition};
    end
    
    regionRelativePoly = currReg.polygon;
    regionRelativePoly.Vertices(:,1) = regionRelativePoly.Vertices(:,1) - regionFuseRef.XWorldLimits(1);
    regionRelativePoly.Vertices(:,2) = regionRelativePoly.Vertices(:,2) - regionFuseRef.YWorldLimits(1);            
        
    [smallerImg,bigImgMask] = resampleImageToNewSpatialRef2(regionFuse,regionFuseRef,RC,'bilinear','MaskOnly');    
        
    %smallMask = poly2mask(regionRelativePoly.Vertices(:,1),regionRelativePoly.Vertices(:,2),regionFuseRef.ImageSize(1),regionFuseRef.ImageSize(2));
    %if numel(smallerImg)~=numel(smallMask)
    %    smallMask = imresize(smallMask,[max(sum(bigImgMask,1)),max(sum(bigImgMask,2))]); %This is needed as there may be rescale between the fused and the big showed img
    %end
    
    %bigImgMask(bigImgMask) = smallMask;
    %C(repmat(bigImgMask,[1 1 nofLayersInBigImg])) = smallerImg(repmat(smallMask,[1 1 nofLayersInBigImg]));
    C(repmat(bigImgMask,[1 1 nofLayersInBigImg])) = smallerImg;
end

C = cast(C,class(Imgs{1}));

end % imshowpair

%-------------------------------------
%--------- Nested Function -----------
%-------------------------------------

function [result,resultRef] = local_createBlend(currImgs,currMasks,fullRegRef,currImgIsRGB,scaling)
    % NOTE: we are only interested in creating the FULL intersection
    % blending        
    
    nofLayers = length(currImgs);
    
    % Creates a transparent overlay image
    
    % make the images similar in size and type    
    [currImgs] = makeSimilar(currImgs,currImgIsRGB,scaling);

    % compute regions of overlap
    outImgSize = fullRegRef.ImageSize;
    allMasks = true(outImgSize);
    
    for i=1:nofLayers
        allMasks = allMasks & currMasks{i};
    end
    
    %Calculating result size and reference
    [desiredSize,~,xmaxi,xmini,~,ymini,ymaxi] = getMaskDim(allMasks);
    
    [xStart,yStart] = fullRegRef.intrinsicToWorld(xmini-.5,ymini-.5);
    [xEnd,yEnd] = fullRegRef.intrinsicToWorld(xmaxi+.5,ymaxi+.5);
        
    resultRef = imref2d(desiredSize, [xStart xEnd], [yStart yEnd] );
        
    weights = zeros(outImgSize(1),outImgSize(2),length(currMasks));
    for i=1:nofLayers
        onlyThis = currMasks{i} & ~allMasks;
        weights(:,:,i) = -bwdist(onlyThis);
    end

    %Normalize weights
    weights = (2/nofLayers)+weights./-repmat(sum(weights,3),[1 1 nofLayers]);
    
    % allocate result image
    result = zeros(desiredSize);

    % for each color band, compute blended output band
    for i = 1:size(currImgs{1},3)
                        
        singleLayers = zeros([outImgSize nofLayers]);
        for j=1:nofLayers
            singleLayers(:,:,j) = currImgs{j}(:,:,i);
        end
        %Only the region of interest               
        
        singleLayers = reshape(singleLayers(repmat(allMasks,[1 1 nofLayers])),[desiredSize nofLayers]);
        locWeights = reshape(weights(repmat(allMasks,[1 1 nofLayers])),[desiredSize nofLayers]);
        
        mergedRegion = zeros(size(locWeights,1),size(locWeights,2));
        for j=1:nofLayers
            mergedRegion = mergedRegion + locWeights(:,:,j).*singleLayers(:,:,j);
        end                
        
        result(:,:,i) = mergedRegion;
                
    end
    %}

end % local_createBlend

%-------------------------------------
%--------- Nested Functions END -----------
%-------------------------------------



function result = local_createRGB(channels)

    % convert any RGB images to grayscales
    if size(A,3) > 1
        A = rgb2gray(A);
    end
    if size(B,3) > 1
        B = rgb2gray(B);
    end

    switch lower(scaling)
        case 'none'
        case 'joint'
            [A,B] = scaleTwoGrayscaleImages(A,B);
        case 'independent'
            A = scaleGrayscaleImage(A);
            B = scaleGrayscaleImage(B);
    end
    A = im2uint8(A);
    B = im2uint8(B);

    % create anaglyph image
    result = zeros([size(A,1) size(A,2) 3], class(A));

    for p = 1:3
        if (channels(p) == 1)
            result(:,:,p) = A;
        elseif (channels(p) == 2)
            result(:,:,p) = B;
        end
    end

end % local_createRGB


function result = local_createDiff

    % convert any RGB images to grayscales
    if size(A,3) > 1
        A = rgb2gray(A);
    end
    if size(B,3) > 1
        B = rgb2gray(B);
    end

    switch lower(scaling)
        case 'none'
        case 'joint'
            [A,B] = scaleTwoGrayscaleImages(A,B);
        case 'independent'
            A = scaleGrayscaleImage(A);
            B = scaleGrayscaleImage(B);
    end

    % create difference image
    result = scaleGrayscaleImage(imabsdiff(single(A),single(B)));
    result = im2uint8(result);

end % local_createDiff

function result = local_createCheckerboard

    [A,B] = makeSimilar(A,B,AisRGB,BisRGB,scaling);

    sz = size(A);
    result = zeros(sz,'like',A);

    check = [1 0; 0 1];
    check = repmat(check,8);
    maskA = logical(imresize(check,sz(1:2),'nearest'));

    if size(A,3) > 1
        maskA = repmat(maskA,[1 1 3]);
    end
    maskB = ~maskA;

    result(maskA) = A(maskA);
    result(maskB) = B(maskB);

end % local_createCheckerboard


function result = local_createSideBySide

    % make the images similar in size and type
    [A,B] = makeSimilar(A,B,AisRGB,BisRGB,scaling);

    % create side by side image
    result = [A B];

end



function [currImgs] = makeSimilar(currImgs,currImgsIsRGB,scaling)

% scale the images
if ~any(currImgsIsRGB)
    % both are grayscale
    switch lower(scaling)
        case 'none'
        case 'joint'
            [currImgs] = scaleTwoGrayscaleImages(currImgs);
        case 'independent'
            for i=1:length(currImgs)
                currImgs{i} = scaleGrayscaleImage(currImgs{i});                
            end
    end
    
elseif all(currImgsIsRGB)
    % both are RGB
    
else %in this case some are RGB and some are not
    for i=1:length(currImgsIsRGB)       
        if ~currImgsIsRGB(i)            
            % convert B to pseudo RGB "gray" image
            if strcmpi(scaling,'None')
                currImgs{i} = repmat(im2uint8(currImgs{i}),[1 1 3]);
            else
                % Scale the sole grayscale image alone
                currImgs{i} = scaleGrayscaleImage(currImgs{i});
                currImgs{i} = repmat(im2uint8(currImgs{i}),[1 1 3]);
            end   
        end
    end
end

% convert to uint8
for i=1:length(currImgs)
    currImgs{i} = im2uint8(currImgs{i});
end

end % makeSimilar


function [Imgs_Padded,Masks,R_output] = calculateOverlayImages(Imgs,Refs)

% First calculate output referencing object. World limits are minimum
% bounding box that contains world limits of both images. Resolution is the
% minimum resolution in each dimension. We don't want to down sample either
% image.

R_output = calcUnionRefSpace(Refs);

Imgs_Padded = cell(length(Imgs),1);
Masks = cell(length(Imgs),1);
for i=1:length(Imgs)
    %Here we need to write a new resampling method to make this scalable (and work)    
    %[Imgs_Padded{i},Masks{i}] = resampleToNewCoordinateSystem(Imgs{i},Refs{i},R_output);
    [Imgs_Padded{i},Masks{i}] = resampleImageToNewSpatialRef2(Imgs{i},Refs{i},R_output,'bilinear','Padded');
end

end

function R_output = calcUnionRefSpace(Refs)

    outputWorldLimitsX = [ min(cell2mat(cellfun( @(c) c.XWorldLimits(1),Refs,'UniformOutput',false))) ...
                           max(cell2mat(cellfun( @(c) c.XWorldLimits(2),Refs,'UniformOutput',false)))];

    outputWorldLimitsY = [ min(cell2mat(cellfun( @(c) c.YWorldLimits(1),Refs,'UniformOutput',false))) ...
                           max(cell2mat(cellfun( @(c) c.YWorldLimits(2),Refs,'UniformOutput',false)))];

    goalResolutionX = min(cell2mat(cellfun( @(c) c.PixelExtentInWorldX,Refs,'UniformOutput',false)));
    goalResolutionY = min(cell2mat(cellfun( @(c) c.PixelExtentInWorldY,Refs,'UniformOutput',false)));

    widthOutputRaster  = ceil(diff(outputWorldLimitsX) / goalResolutionX);
    heightOutputRaster = ceil(diff(outputWorldLimitsY) / goalResolutionY);

    R_output = imref2d([heightOutputRaster, widthOutputRaster]);
    R_output.XWorldLimits = outputWorldLimitsX;
    R_output.YWorldLimits = outputWorldLimitsY;

end

function [paddedImg,mask] = resampleToNewCoordinateSystem(imgData,imgRef,outRef,bigImg)
%NOTE: FOR SPEED ISSUES WE DISCARD PIXEL EXTENT INFORMATION
%THIS NEEDS REWRITING (%TODO)

    if nargin<4
        paddedImg = zeros(outRef.ImageSize(1),outRef.ImageSize(2));        
    else
        paddedImg = bigImg;
    end
    mask = zeros(outRef.ImageSize(1),outRef.ImageSize(2));
    
    %Shift starting point to origo (0,0)
    startInXWorld = (imgRef.XWorldLimits(1) - outRef.XWorldLimits(1));
    startInPixXWorld = round(startInXWorld/outRef.PixelExtentInWorldX);
    if startInPixXWorld<1, startInPixXWorld = 1; end
    imgSizeX = imgRef.ImageSize(2);
        
    %Shift starting point to origo (0,0)
    startInYWorld = (imgRef.YWorldLimits(1) - outRef.YWorldLimits(1));
    startInPixYWorld = round(startInYWorld/outRef.PixelExtentInWorldY);
    if startInPixYWorld<1, startInPixYWorld = 1; end
    imgSizeY = imgRef.ImageSize(1);
    
    paddedImg(startInPixYWorld:startInPixYWorld+imgSizeY-1,startInPixXWorld:startInPixXWorld+imgSizeX-1) = imgData;
    paddedImg = cast(paddedImg,class(imgData));
    mask(startInPixYWorld:startInPixYWorld+imgSizeY-1,startInPixXWorld:startInPixXWorld+imgSizeX-1) = 1;
end


%----------------------------------------------------
function image_data = scaleGrayscaleImage(image_data)

if (islogical(image_data))
    return
end

% convert to floating point
image_data = single(image_data);

% translate to put minimum at zero
image_data = image_data - min(image_data(:));

% scale to range [0 1]
image_data = image_data / max(image_data(:));

end % scaleGrayscaleImage


%--------------------------------------------
function currImgs = scaleTwoGrayscaleImages(currImgs)
% takes 2 same size images and scales them as a single dataset

% convert to floating point composite image
image_data = cat(2,currImgs{:});

% scale as a single image
image_data = scaleGrayscaleImage(image_data);

% split images
posCounter = 1;
for i=1:length(currImgs)
currImgs{i} = image_data(:,posCounter:posCounter+size(currImgs{i},2)-1,:);
posCounter = posCounter + size(currImgs{i},2);
end

end % scaleTwoGrayscaleImages


function [Refs,vA] = preparseSpatialRefObjects(varargin)

vA = varargin{:};
spatialRefPositions  = cellfun(@(c) isa(c,'imref2d'), vA);

Refs = [];

if ~any(spatialRefPositions)
    return
end

lastRefIdx = find(spatialRefPositions, 1, 'last' );
supposedRefIdx = 2:2:lastRefIdx;

if ~isequal(find(spatialRefPositions), supposedRefIdx)
    error(message('images:imfuse:invalidSpatiallyReferencedSyntax','imref2d'));
end

spatialRef3DPositions   = cellfun(@(c) isa(c,'imref3d'), vA);
if any(spatialRef3DPositions)
    error(message('images:imfuse:imref3dSpecified','imref3d'));
end

Refs = vA(supposedRefIdx);
vA(supposedRefIdx) = [];

end

%---------------------------------------------------------
function [Imgs,Refs,method,options] = parse_inputs(varargin)

% We pre-parse spatial referencing objects before we start input parsing so that
% we can separate spatially referenced syntax from other syntaxes. 
vA = varargin{1};
[Refs,vA] = preparseSpatialRefObjects(vA);

%Parse images as well
firstCharIdx = find( cellfun(@(c) isa(c,'char'), vA), 1, 'first');
if isempty(firstCharIdx), lastImgIdx = length(vA); else, lastImgIdx = firstCharIdx - 1; end
Imgs = vA(1:lastImgIdx);
for i=1:length(Imgs)
    checkA(Imgs{i});
end
vA(1:lastImgIdx) = [];

checkStringInput = @(x,name) validateattributes(x, ...
    {'char','string'},{'scalartext'},mfilename,name);
parser = inputParser();
parser.addOptional('method','falsecolor',@checkMethod);
parser.addParameter('Scaling', 'independent', @(x) checkStringInput(x,'Scaling'));
parser.addParameter('ColorChannels', '', @checkColorChannels1);


% parse input
% Put method at function scope so that we can modify partial string matches
% and replace them with the full string when @checkMethod runs.
method = 'falsecolor';
parser.parse(vA{:});

options.Scaling = checkScaling(parser.Results.Scaling);
options.ColorChannels = checkColorChannels2(parser.Results.ColorChannels);

% After parsing inputs, check consistency.
if (~isempty(options.ColorChannels) && ~strcmp(method, 'falsecolor'))
    error(message('images:imfuse:methodColorChannelCombo'))
end

% Use green-magenta as a default false-coloring.
if isempty(options.ColorChannels)    
    options.ColorChannels = [2 1 2];
end

isSpatiallyReferencedSyntax = ~isempty(Refs);

if isSpatiallyReferencedSyntax
    % If spatial referencing objects are user specified, make sure sizes
    % match the input image data.
    for i=1:lastImgIdx
        Refs{i}.sizesMatch(Imgs{i});
    end
else
    for i=1:lastImgIdx
        Refs{i} = imref2d(size(Imgs{i}));    
    end
end

    function TF = checkA(A)
        
        validateattributes(A,{'numeric','logical'},{'real','nonsparse','nonempty'},mfilename,'A')
        TF = true;
        
        %Get rid of dim restriction
        %A_bands = size(A,3);
        %if (ndims(A) > 3) || ((A_bands ~= 1) && (A_bands ~= 3))
        %    error(message('images:imfuse:unsupportedDimension'));
        %end
        
    end

    function str = checkScaling(user_param)
        str = validatestring(user_param, ...
            {'joint','independent','none'},mfilename,'Scaling');
    end

    function TF = checkMethod(methodArg)
        
        valid_methods = {'falsecolor','diff','blend','montage','checkerboard'};
        method = validatestring(methodArg,valid_methods,mfilename,'METHOD');
        
        TF = true;
        
    end

    function checkColorChannels1(arg)
        % Check numeric values
        if ~ischar(arg) && ~isstring(arg)
            % First-pass validation using validateattributes().
            attributes = {'>=', 0, '<=', 2, 'finite', 'integer', 'vector', 'nonsparse', 'numel', 3};
            validateattributes(arg, {'numeric'}, attributes, mfilename, 'ColorChannels')

            % Validate that both 1 and 2 appear in the P-V value.
            if ((numel(find(arg == 1)) > 2) || (numel(find(arg == 1)) < 1) || ...
                (numel(find(arg == 2)) > 2) || (numel(find(arg == 2)) < 1))
                error(message('images:imfuse:invalidColorChannels'))
            end
        end
    end

    function out = checkColorChannels2(in)
        % Check char/string input
        if ischar(in) || isstring(in)
            str = validatestring(in, ...
                {'red-cyan','green-magenta',''}, ...
                mfilename, 'ColorChannels');
            switch (str)
                case 'green-magenta'
                    out = [2 1 2];
                case 'red-cyan'
                    out = [1 2 2];
                otherwise
                    out = [];
            end
        else
            out = in;
        end
    end

end % parse_inputs
