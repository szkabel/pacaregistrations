function [regionMap] = identifyRegions(Refs)
% This function takes an imref2d cellarray and returns an array where each
% region created by the rectangles are presented. (e.g. if 2 imref2d
% rectangle overlaps than it should create 3 'regions': 2 for those where
% only one single is present and 1 for the intersection). The regions must
% contain the following information:
%
% The region must contain the following info:
%   .polygon (it is required, rectangle is not enough).
%   .composition a list of the indices of the images composing the region
%
% Algorithm: it adds region iteratively so that it ensures that after one
% addition the regions are non-overlapping.

regionMap = containers.Map('KeyType','uint64','ValueType','any');

%For each of these identify their overlap
for i=1:length(Refs)
    soFarKeys = regionMap.keys();
    currRef = Refs{i};
    currRefPoly = polyshape(currRef.XWorldLimits([1 1 2 2]) , currRef.YWorldLimits([1 2 2 1]) );
    if ~isempty(soFarKeys), maxKey = max([soFarKeys{:}]); else, maxKey = 0; end
    for j=1:length(soFarKeys)
        roi = regionMap(soFarKeys{j});
        interSec = intersect(roi.polygon,currRefPoly);
        if ~isempty(interSec.Vertices)
            newRoiPoly = subtract(roi.polygon,currRefPoly);
            newCurr = subtract(currRefPoly,roi.polygon);
            currRefPoly = newCurr;
            roi.polygon = newRoiPoly;
            
            regionMap(soFarKeys{j}) = roi;
            
            intersectRegion = struct(...
                'polygon',interSec,...
                'composition',[i roi.composition]);
            
            maxKey = maxKey + 1;
            regionMap(maxKey) = intersectRegion;
        end
    end
    remainingRegion = struct(...
        'polygon',currRefPoly,...
        'composition',i);
    maxKey = maxKey + 1;
    regionMap(maxKey) = remainingRegion;
end
    
end
