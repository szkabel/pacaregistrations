%This script roughly registers the images to each other and saves them to
%the required output folder.
% Its input is provided in pyInputBase folder
% It should write out the Original Image in Original Size to the
% RoughRegistered folder
% In each case the registration will be done with the python framework (works the best)
% The preparation modules (generating the images into the pyInputBase folder) should take care themselves
% about downsampling the image with factor roughRescaleFactor

regGraphs = cell(N,1);
regTransform = cell(N,1);

roughRegMatDir = fullfile(targetDir,'roughRegMatrices');
if ~exist(roughRegMatDir,'dir'), mkdir(roughRegMatDir); end

%Pariwise registration in parallel
parfor i=1:length(setIDs)
    %the current image set
    %imgSet = loadImgSet(fullInputFolders,formatStrings,setIDs(i));
    regGraphs{i} = zeros(nofLayers);
    regTransform{i} = cell(nofLayers);
    
    %Do pairwise distance comparison
    for ii = 1:size(nodeList,1)
        j = nodeList(ii,1);
        k = nodeList(ii,2);
        %Prepare file paths
        pwRegDir = fullfile(roughTgtDir,sprintf('%d_%d',j,k));
        if ~isfolder(pwRegDir), mkdir(pwRegDir); end        
        
        refFileName = sprintf(formatStrings{k},setIDs(i)); %#ok<PFBNS>
        fixFileName = sprintf(formatStrings{j},setIDs(i));
        [~,reffnexex,refintExt] = fileparts(refFileName);
        [~,fixfnexex,fixintExt] = fileparts(fixFileName);
        if isnumeric(chToUseForRoughReg)            
            refImgFileName = [reffnexex '_ch' num2str(chToUseForRoughReg,'%02d') refintExt];
            fixImgFileName = [fixfnexex '_ch' num2str(chToUseForRoughReg,'%02d') fixintExt];
        elseif strcmp(chToUseForRoughReg,'ilastik')
            refImgFileName = sprintf(formatStrings{k},setIDs(i));
            fixImgFileName = sprintf(formatStrings{j},setIDs(i));
        end
        refImgPath = fullfile(pyInputBase,inputFolders{k},refImgFileName); %#ok<PFBNS>
        fixImgPath = fullfile(pyInputBase,inputFolders{j},fixImgFileName);
        tmpDir = fullfile(targetDir,'tmp',reffnexex);
        currOutDir = fullfile(roughRegMatDir,sprintf('%d_%d',j,k));
        if ~exist(currOutDir,'dir'), mkdir(currOutDir); end
        outFilePath = fullfile(currOutDir,['RoughRegistered_' reffnexex '.txt']);
        
        %Run the registration
        system([prePythonCall '; ' ...
	    pythonLocation ' ' fullfile(pyRegLoc,'register_example2.py') ' '...            
            fixImgPath ' ' ...
            refImgPath ' ' ...
            tmpDir ' ' ...
            outFilePath ' ' ...
            ],'-echo');
        
        fixOrigImg = imread(fullfile(fullInputFolders{j},sprintf(formatStrings{j},setIDs(i))));                 %#ok<PFBNS>
        fixOrigImgHSV = rgb2hsv(fixOrigImg);
        fixOrigImg_sat = fixOrigImgHSV(:,:,2);
        refOrigImg = imread(fullfile(fullInputFolders{k},sprintf(formatStrings{k},setIDs(i))));        
        refOrigImgHSV = rgb2hsv(refOrigImg);
        refOrigImg_sat = refOrigImgHSV(:,:,2);
        refOrigImg_satTrim = imwarp(refOrigImg_sat,affine2d(eye(3)),'OutputView',imref2d(size(fixOrigImg_sat)));
        oldCorr = corr2(refOrigImg_satTrim,fixOrigImg_sat);
        
        prepRefImg = imread(refImgPath);
        prepFixImg = imread(fixImgPath);
        
        roughRegistrationMatrix = load(outFilePath);
        currRoughTform = convertPythonRawTrafoToMatlab(roughRegistrationMatrix,size(prepRefImg),size(prepFixImg));
        upScaleTrafo = [1/roughRescaleFactor 0 0; 0 1/roughRescaleFactor 0; 0 0 1];
        downScaleTrafo = [roughRescaleFactor 0 0; 0 roughRescaleFactor 0; 0 0 1];
        currRoughTform.T = downScaleTrafo * currRoughTform.T * upScaleTrafo; %Scale back up the trafo
        roughRegImg_sat = imwarp(refOrigImg_sat,currRoughTform,'OutputView',imref2d(size(fixOrigImg_sat)));
        newCorr = corr2(roughRegImg_sat,fixOrigImg_sat);
                        
        if newCorr > oldCorr 
            regGraphs{i}(j,k) = newCorr;
            regTransform{i}{j,k} = currRoughTform;
        else
            regGraphs{i}(j,k) = oldCorr;
            regTransform{i}{j,k} = affine2d(eye(3));
        end
        improvement = (newCorr-oldCorr)/abs(oldCorr)*100;
        fprintf('The old correlation was: %f\nThe new correlation is: %f\nThe improvement is: %4.2f%%\n',oldCorr,newCorr,improvement);
                                
        imgReg = imwarp(refOrigImg,regTransform{i}{j,k},'OutputView',imref2d(size(fixOrigImg)));
        imwrite(imgReg,fullfile(pwRegDir,['Registered_' inputFolders{k} '_F' num2str(setIDs(i),'%02d') ext])); 
                
    end
    fprintf('Set %d done (%d/%d)\n',setIDs(i),i,N);
    
end

save(fullfile(targetDir,'roughRegistered_MetaData.mat'),'regTransform','regGraphs');
