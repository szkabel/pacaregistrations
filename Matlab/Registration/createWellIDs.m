function [wellIDarray] = createWellIDs(row,col)

wellIDarray = cell(row,col);
letter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

if row>length(letter)
    row = length(letter);
end

for i=1:row
    for j=1:col
        wellIDarray{i,j} = [ letter(i) num2str(j)];
    end
end
end

