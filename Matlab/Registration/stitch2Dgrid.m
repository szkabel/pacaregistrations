function [stitchedImg] = stitch2Dgrid(imgStruct,pixelSizes,sL4Estim,sL4Stitch,nRow,nCol,varargin)
%Img struct is a cellarray with structures with the following fields:
%   .data: image data which is a cellarray for multiple layers (has nofLayers length)
%   .xPos: position X in a standardized measure
%   .yPos: position Y in the same measure
%
% pixelSizes: the physical size of 1 pixel in the same measure as posX
% in x and y direction (hence this is a vector of length 2)
%
% sL4Estim and sL4Stitch stands for selected layers for estimation and
% stiching.
% sL4Estim is a 2 by N (where N is the number of layers) matrix for the
% weights and gammas of the averaging.
% sl4Stitching is a regular array with indices for the layers to be
% included in the final stitchedImg.
% If the images are multi layered then the first variable
% (estimation) refers to those layers whom are going to be averaged out to
% identify the registration. Then the layers 4 stitching are the actual
% layers that will be included in the final stitchedImg output.
%
% Run the registration on JOINT (averaged) layers
%
% NOTE: stitchedImg is a cellarray with the entries being the separated
% selected layers for stitching.
%
% nRow,nCol: the number of rows and columns in the stitching

p = inputParser();
p.addParameter('writeFile',false);
p.addParameter('filePattern','FILE_NAME_NOT_GIVEN_%02d');
p.parse(varargin{:});

N = length(imgStruct);
posX = zeros(N,1);
posY = zeros(N,1);

downScaleFactor = 10;

[optimizer,metric] = imregconfig('monomodal');

%Get order right
for i=1:N
    posX(i) = imgStruct{i}.xPos/downScaleFactor;
    posY(i) = imgStruct{i}.yPos/downScaleFactor;
end

finalSortList = zeros(N,1);
[~,sortYidx] = sort(posY,'descend');
for i=1:nRow
    currRowIdx = sortYidx((i-1)*nCol+1:i*nCol);
    [~,innerSrtIdx] = sort(posX(currRowIdx),'ascend');
    innerIdxList = currRowIdx(innerSrtIdx);
    finalSortList((i-1)*nCol+1:i*nCol) = innerIdxList;
end

%The first ever should be the origo
currIdx = finalSortList(1);
stitchedImgEst = averageLayers(imgStruct{currIdx}.data,downScaleFactor,sL4Estim);
stitchReference = imref2d(size(stitchedImgEst));
prevTrfm = cell(N,1);
prevTrfm{1} = affine2d(eye(3));
prevRowStartIdx = currIdx;

%Seems to be an issue and should be handled later
warning('error','images:regmex:registrationOutBoundsTermination'); %#ok<CTPCT>
warning('error','images:imregcorr:weakPeakCorrelation'); %#ok<CTPCT>

for i=2:N            
    fprintf('Estimating registration (stitching) [%03d/%d] ... ',i,N);
    currIdx = finalSortList(i);
    %If new row
    if mod(i-1,nCol) == 0
        prevIdx = prevRowStartIdx;
        prevRowStartIdx = currIdx;
    %Normal case growing in row
    else                       
        prevIdx = finalSortList(i-1);
    end

    origDeltaTfm = affine2d([...
            1                                           0                                           0;...
            0                                           1                                           0;...
            (posX(currIdx)-posX(prevIdx))/pixelSizes(1) (posY(currIdx)-posY(prevIdx))/pixelSizes(2) 1]);
    initTrfm = affine2d(prevTrfm{prevIdx}.T*origDeltaTfm.T);


    movingImg = averageLayers(imgStruct{currIdx}.data,downScaleFactor,sL4Estim);
    movingRef = imref2d(size(movingImg));
    
    [warpedImg,warpedRef] = imwarp(movingImg,movingRef,initTrfm);
    fprintf(' phase correlation: ');
    try
        currTform = imregcorr(...
            warpedImg,warpedRef,...
            stitchedImgEst,stitchReference,...
            'translation'...
            );                
        fprintf('Y');
    catch e        
        if strcmp(e.identifier,'images:imregcorr:weakPeakCorrelation')
            currTform = affine2d(eye(3));
            fprintf('N');
        else
         %   rethrow(e);
        end
    end
    fprintf(' iterative: ');
    try    
        currTformIt = imregtform(...
            warpedImg,warpedRef,...
            stitchedImgEst,stitchReference,...
            'translation',...
            optimizer,...
            metric,...
            'InitialTransformation',currTform...
            );
        currTform = currTformIt;
        fprintf('Y');
    catch e
        if strcmp(e.identifier,'images:regmex:registrationOutBoundsTermination')            
            fprintf('N');
        else
            %rethrow(e);
        end
    end    
    fprintf('  Warping...');

    [warpedImg,warpedRef] = imwarp(warpedImg,warpedRef,currTform);
  
    [stitchedImgEst,stitchReference] = imfuse3(stitchedImgEst,stitchReference,warpedImg,warpedRef,'method','blend','Scaling','none');

    fprintf('\n');
    currTform = affine2d(eye(3));
    
    prevTrfm{currIdx} = affine2d(initTrfm.T*currTform.T);    
        
end

warning('off','images:regmex:registrationOutBoundsTermination');

%Finally do stitching

%First calculate the reference and warped version of each image
warpImgsParams = cell(2*N,length(sL4Stitch));

for i=1:N    
    currIdx = finalSortList(i);
    movingImg = cat(3,imgStruct{currIdx}.data{sL4Stitch});
    movingRef = imref2d(size(movingImg));
    
    currTform = prevTrfm{currIdx};
    currTform.T(3,[1 2]) = currTform.T(3,[1 2])*downScaleFactor;
    [warpImg,warpRef] = imwarp(movingImg,movingRef,currTform);
        
    for j=1:length(sL4Stitch)
        warpImgsParams{2*(i-1)+1,j} = warpImg(:,:,j);
        warpImgsParams{2*(i-1)+2,j} = warpRef;    
    end
    
end

stitchedImg = cell(length(sL4Stitch),1);

clear('imgStruct');
clear('warpImg');
clear('warpRef');
pause(1); % some time hopefully for garbadge collector
save('200408_tmpSave','warpImgsParams');

for j=1:length(sL4Stitch)    
    thisLayer = imfuse2(warpImgsParams{:,j},'method','blend','Scaling','none');    
    if p.Results.writeFile
        [filePath,fileExEx,ext] = fileparts(p.Results.filePattern);
        imwrite(thisLayer,fullfile(filePath,[sprintf(fileExEx,j) ext]));
        clear('thisLayer');
    else
        stitchedImg{j} = thisLayer;
    end
    pause(1); %again hopefully for garbadge collector. The actual space should not be taht much
end
    

end
