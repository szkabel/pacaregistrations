%To plot the color deconvolution histograms
% Note: this code considers channel 1 as the stain of interest (so the first channel should be the "brown" stain)
% deconvHists should be loaded (saved out to deconvHists.mat by colorDeconvolveSet)

chanOfInterest = 1;
barGap = 5000;

figure;
[rowC,colC] = closestDivisor(nofLayers);

for j=1:nofLayers
    currStainHC = deconvHists{j}.HC(1,:,chanOfInterest);
    currEdges = deconvHists{j}.edges{chanOfInterest};
    
    firstIdx = find(currStainHC ~= 0,1,'first');
    lastIdx = find(currStainHC ~= 0,1,'last');
    currNonZeroStainHC = currStainHC(firstIdx:lastIdx);
    currNonZeroEdges = currEdges(firstIdx:lastIdx+1);
    nofBars = length(currNonZeroStainHC);
        
    subplot(rowC,colC,j);
    bar(currNonZeroStainHC);
    tickArray = 1:barGap:nofBars;
    xticks(tickArray);
    xTL = cell(length(tickArray),1);
    for i=1:length(tickArray)
        xTL{i} = num2str(currNonZeroEdges(tickArray(i)),'%2.2f');
    end
    xticklabels(xTL);
    set(gca, 'YScale', 'log');
    title(sprintf('Stain distribution of %s',strsplitN(inputFolders{j},'_',2)));
end