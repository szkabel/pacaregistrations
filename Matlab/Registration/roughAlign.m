function geoTransf = roughAlign(imgFixed,imgMoving)

    [opt,metric] = imregconfig('monomodal');
    %imgMoving = imhistmatch(imgMoving,imgFixed); %No point in histogram
    %match as these are binary in this case
    geoTransf = imregtform(imgMoving,imgFixed,'similarity',opt,metric);
    %geoTransf = imregcorr(imgFixed,imgMoving,'rigid');

end