tifsrc = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\SHG_HE\HE';
tiftgt = fullfile(tifsrc,'renamed');
lifsrc = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\SHG_HE\SHG';
liftgt = fullfile(lifsrc,'renamed');

if ~exist(tiftgt,'dir'); mkdir(tiftgt); end
if ~exist(liftgt,'dir'); mkdir(liftgt); end

%Go one by one
renameTable = readtable('D:\Abel\SZBK\Projects\Juho\PancreaticCancer\SHG_HE\renaming\renameToSameSpace.csv','ReadVariableNames',true,'Delimiter',',');

startI = 7;
for i=startI:floor(size(renameTable,1)/2)

    lifIdx = (2*(i-1))+1;
    tifIdx = (2*(i-1))+2;
    %We'll always rotate the TIF as the lif is difficult to handle
    tifImg = imread(fullfile(tifsrc,renameTable.Original{tifIdx}));
    lifImg = bfopen2(fullfile(lifsrc,renameTable.Original{lifIdx}),[-1,0]);

    figure('Units','Normalized','Position',[0 0 1 1]);
    subplot(2,2,1);
    imagesc(tifImg);
    subplot(2,2,3);
    imagesc(lifImg{end,1}{4,1});
    subplot(2,2,2);
    imagesc(lifImg{end,1}{2,1});

    subplot(2,2,4);
    theta = pi/2;
    T = affine2d([cos(theta) sin(theta) 0; sin(theta) cos(theta) 0; 0 0 1]);
    trfmdTif = imwarp(tifImg,T);
    imagesc(trfmdTif);

    res = questdlg('Can we save this?','Save title','Yes','No','Yes');
    if strcmp(res,'Yes')
        imwrite(trfmdTif,fullfile(tiftgt,renameTable.New{tifIdx}));
        movefile(fullfile(lifsrc,renameTable.Original{lifIdx}),fullfile(liftgt,renameTable.New{lifIdx}));
    else
        break;
    end
end