%Script to plot channel block sizes.
%First the blockSizeData variable (saved down by collocalizeSet script with name: channelBlockSizes.mat)

if ~exist('blockSizeData','var')
    load('G:\Abel\SZBK\Projects\Juho\PancreaticCancer\TMAs\fullRun\channelBlockSizes.mat');
end

nofChannels = length(blockSizeData);
[r,c] = closestDivisor(nofChannels);

figure;

blockSizeMed = zeros(nofChannels,1);
blockSizeMean = zeros(nofChannels,1);

for i=1:nofChannels
    subplot(r,c,i);
    histogram(cell2mat(blockSizeData{i}(:,2)))
    title(strrep(inputFolders{i},'_','-'));
    blockSizeMed(i) = median(cell2mat(blockSizeData{i}(:,2)));
    blockSizeMean(i) = mean(cell2mat(blockSizeData{i}(:,2)));
end