%prepIlastikTrainingSet

minImgCount = 5;
trainPercent = 0.05; % 5 percent

for j=1:nofLayers
    currDirName = fullfile(ilastikTrainDir,inputFolders{j});
    if ~exist(currDirName,'dir'), mkdir(currDirName); end
    
    randOrd = randperm(N);
    nofTrainingSamples = max(round(N*trainPercent),minImgCount);
    selectedIdx = randOrd(1:nofTrainingSamples);
    
    for i = 1:nofTrainingSamples
        img = imread(fullfile(fullInputFolders{j},sprintf(formatStrings{j},setIDs(selectedIdx(i)))));        
        imwrite(imresize(img,roughRescaleFactor),fullfile(currDirName,sprintf(formatStrings{j},setIDs(selectedIdx(i)))));
    end
end