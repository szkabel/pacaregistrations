%run ilastik

for j=usedLayers'
    
    currOutDir = fullfile(ilastikResDir,inputFolders{j});
    currTmpDir = fullfile(ilastikTmpDir,inputFolders{j});
    
    if ~exist(currOutDir,'dir'), mkdir(currOutDir); end
    if ~exist(currTmpDir,'dir'), mkdir(currTmpDir); end
    
    fullImgNames = cell(N,1);
    parfor i=1:N
        fullOrigImgName = fullfile(fullInputFolders{j},sprintf(formatStrings{j},setIDs(i)));
        img = imread(fullOrigImgName);
        fullImgNames{i} = fullfile(currTmpDir,sprintf(formatStrings{j},setIDs(i)));
        imwrite(imresize(img,roughRescaleFactor),fullImgNames{i});
    end
    
    system([...
        '"' ilastikLocation '" '...
        '--headless '...
        '--project="' fullfile(ilastikTrainDir,[inputFolders{j} '.ilp']) '" ' ...
        '--output_format=tif '...
        '--output_filename_format="' currOutDir filesep '{nickname}.tif" ' ...
        '"' strjoin(fullImgNames,'" "') '"'...
        ],'-echo');
        
    for i=1:N
        fullResImgName = fullfile(currOutDir,sprintf(formatStrings{j},setIDs(i)));
        img = imread(fullResImgName);
        imwrite(double(img(:,:,1)),fullResImgName);
    end
end
