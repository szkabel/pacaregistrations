function [imgStruct,pixelSizes,nRow,nCol] = convertLifArrayToImageStruct(shgBF)
% shgBF cellarray as loaded by Bioformats

imgIdx = 1; %Doesn't matter it's all the same anyway
res = strsplit(string(shgBF{imgIdx,2}.toString()),',')'; 

resIdx = contains(res,'Image|Tile|FieldY'); 
%For some reason it is actually 1 more
nCol = str2double(strsplitN(res(resIdx),'=',2))+1;

resIdx = contains(res,'Image|Tile|FieldX'); 
nRow = str2double(strsplitN(res(resIdx),'=',2))+1;

N = nCol*nRow;
imgStruct = cell(nCol*nRow,1);


for i=1:N
    imgStruct{i}.data = shgBF{i,1}(:,1);
    %Burnt in: xPos are in meters -> convert to micrometer
    imgStruct{i}.xPos = str2double(strsplitN(string(shgBF{1,4}.getPlanePositionX(i-1,0).toString),{'[',']'},2))*10^6;
    imgStruct{i}.yPos = str2double(strsplitN(string(shgBF{1,4}.getPlanePositionY(i-1,0).toString),{'[',']'},2))*10^6;
    
end

%Supposing equal pixel size for each image
pixelSizes = [...
        str2double(strsplitN(string(shgBF{1,4}.getPixelsPhysicalSizeX(0).toString),{'[',']'},2))...
        str2double(strsplitN(string(shgBF{1,4}.getPixelsPhysicalSizeY(0).toString),{'[',']'},2))...
        ];

end

