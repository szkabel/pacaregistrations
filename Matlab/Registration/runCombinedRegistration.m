% NOTE: this script is the continuation of the runRoughRegistration, it needs
% certain variables from that file.
% The recommended scenario is to run first the runRoughRegistration script
% then the python script (smoother registration) then this one to produce
% the final registrations.


pythonSmoothDir = fullfile(targetDir,'SmoothRegistrations');

finalFolder = fullfile(targetDir,'FinalRegistrations');
finalFolderSmall = fullfile(targetDir,'FinalRegistrationsSmall');

if ~isfolder(finalFolder), mkdir(finalFolder); end
if ~isfolder(finalFolderSmall), mkdir(finalFolderSmall); end

if ~exist(fullfile(targetDir,'smoothRegistered_MetaData.mat'),'file')

%  if exist(fullfile(targetDir,'myDebugOfCombined.mat'),'file')
%    load(fullfile(targetDir,'myDebugOfCombined.mat'),'regTransform','regGraphs');
%  else
    for iii = 1:size(nodeList,1)
        j = nodeList(iii,1);
        k = nodeList(iii,2);
        %These both should exist
        pwRegRoughDir = fullfile(roughTgtDir,sprintf('%d_%d',j,k));
        pwRegSmoothDir = fullfile(pythonSmoothDir,sprintf('%d_%d',j,k));        
        
        parfor ii=1:length(setIDs)
            i = setIDs(ii);
            fprintf('Processing image set %d in scenario %d_%d\n',i,j,k);
            currOriginalImg = loadImgSet(fullInputFolders(j),formatStrings(j),i);
            currOriginalImg = currOriginalImg{1};
            currRegT = regTransform{ii};
            currRegG = regGraphs{ii}

            currOrigMovingImg = imread( fullfile(fullInputFolders{k},sprintf(formatStrings{k},i)));
            roughRegisteredImg = imwarp(currOrigMovingImg,currRegT{j,k},'OutputView',imref2d(size(currOriginalImg)));         
            smoothRegistrationMatrix = load( fullfile(pwRegSmoothDir,['SmoothTransform_' inputFolders{k} '_F' num2str(i,'%02d') '.txt' ]));

            %Plan B
            %displacementField = convertPythonRawToDisplacementField(smoothRegistrationMatrix,size(roughRegisteredImg));
            %smoothRegImg = imwarp(roughRegisteredImg,displacementField);
            %
            % NOTES: even with the greatest efforts the exact python
            % transformation can only be reconstructed with plan B (i.e. following directly their implementation)
            % Due to lack of documentation and expertise the Plan A version
            % (implementing the transformation as matrix multiplication)
            % still has some small differences compared to the output of
            % python. The reason is unknown.

            %Plan A
            currSmoothTform = convertPythonRawTrafoToMatlab(smoothRegistrationMatrix,size(roughRegisteredImg),size(currOriginalImg));
            smoothRegImg = imwarp(roughRegisteredImg,currSmoothTform,'OutputView',imref2d(size(roughRegisteredImg)));

            smoothRegImgHSV = rgb2hsv(smoothRegImg);
            smoothRegImg_sat = smoothRegImgHSV(:,:,2);
            currOriginalImgHSV = rgb2hsv(currOriginalImg);
            currOriginalImg_sat = currOriginalImgHSV(:,:,2);
            roughRegisteredImgHSV = rgb2hsv(roughRegisteredImg);
            roughRegisteredImg_sat = roughRegisteredImgHSV(:,:,2);

            if ~all(size(smoothRegImg_sat) == size(currOriginalImg_sat))
                smoothRegImg_sat = imwarp(smoothRegImg_sat,affine2d(eye(3)),'OutputView',imref2d(size(currOriginalImg_sat)));
            end
            newCorr = corr2(smoothRegImg_sat,currOriginalImg_sat);            
            %Old correlation values should be stored in the regGraphs file
            %but just as a sanity check
            if ~all(size(roughRegisteredImg_sat) == size(currOriginalImg_sat))
                roughRegisteredImg_sat = imwarp(roughRegisteredImg_sat,affine2d(eye(3)),'OutputView',imref2d(size(currOriginalImg_sat)));
            end
            oldCorr = corr2(roughRegisteredImg_sat,currOriginalImg_sat);

            if abs(oldCorr - currRegG(j,k))>0.01
                warning('Correlation mismatch!'); 
            end

            if newCorr > oldCorr
                currRegG(j,k) = newCorr; % ok<SAGROW> this is preallocated in another script
                currRegT{j,k}.T = currRegT{j,k}.T*currSmoothTform.T; % ok<SAGROW> this is preallocated in another script
                fprintf('Improvement! From %f to %f\n',oldCorr,newCorr);
            end

	    regTransform{ii} = currRegT;
	    regGraphs{ii} = currRegG;

        end            
    end

 %   save(fullfile(targetDir,'myDebugOfCombined.mat'),'regTransform','regGraphs');
 % end

    % Here comes the interesting part:)
    % Calculate the maximum spanning tree on the graph and select the node with
    % the highest degree (just for fun) in the graph as the fixed img. Then transform all the
    % others to this fixed img. (Technically minimum spanning tree)

    finalTransforms = cell(1,length(setIDs));
    spanTrees = cell(1,length(setIDs));
    initIdx = cell(1,length(setIDs));

    parfor i=1:length(setIDs) 
        finalTransforms{i}.ID = setIDs(i);
        weightMatrix = ones(nofLayers) - (regGraphs{i} + regGraphs{i}') - diag(ones(1,nofLayers));
	weightMatrix = weightMatrix(usedLayers,usedLayers);
        G = graph(weightMatrix);
        T = minspantree(G);
        spanTrees{i} = T;
        [~,maxidx] = max(T.degree);
        nodeOrder = dfsearch(T,maxidx,'edgetonew');
	nodeOrder = usedLayers(nodeOrder);
        finalTransforms{i}.tforms = cell(1,nofLayers);    
        %Init transforms
        for j = 1:nofLayers, finalTransforms{i}.tforms{j} = affine2d(eye(3)); end
        for j = 1:size(nodeOrder,1)
            if isempty(regTransform{i}{nodeOrder(j,1),nodeOrder(j,2)})
                newTrafo = finalTransforms{i}.tforms{nodeOrder(j,1)}.T*inv(regTransform{i}{nodeOrder(j,2),nodeOrder(j,1)}.T);  %#ok<MINV>
                newTrafo(1:2,3) = 0; %Correct to meet assert            
                newTrafo(3,3) = 1;
                finalTransforms{i}.tforms{nodeOrder(j,2)}.T = newTrafo; 
            else
                newTrafo = finalTransforms{i}.tforms{nodeOrder(j,1)}.T*regTransform{i}{nodeOrder(j,1),nodeOrder(j,2)}.T;
                newTrafo(1:2,3) = 0; %Correct to meet assert
                newTrafo(3,3) = 1;
                finalTransforms{i}.tforms{nodeOrder(j,2)}.T = newTrafo; 
            end        
        end
        initIdx{i} = nodeOrder(1,1);
    end
    
    locTable = table([],[],'VariableNames',{'setID','scoreSum'});
    for i=1:length(setIDs), singleRow = table(setIDs(i),sum(spanTrees{i}.Edges.Weight)); locTable(i,:) = singleRow; end
    writetable(locTable,fullfile(targetDir,'jointRegistrationScores.csv'));

    %Save down some parameters for further usage
    save(fullfile(targetDir,'smoothRegistered_MetaData.mat'),'regTransform','regGraphs','finalTransforms','spanTrees','initIdx');
else
    load(fullfile(targetDir,'smoothRegistered_MetaData.mat'),'regTransform','regGraphs','finalTransforms','spanTrees','initIdx');
end

fprintf('Warping images...\n');

%Actually transform
parfor i=1:length(setIDs) 
    imgSet = loadImgSet(fullInputFolders,formatStrings,setIDs(i));
    finalTgtDir = fullfile(finalFolder,num2str(setIDs(i),'%02d'));
    if ~isfolder(finalTgtDir), mkdir(finalTgtDir); end
    for j=usedLayers'
        warpedImg = imwarp(imgSet{j},finalTransforms{i}.tforms{j},'OutputView',imref2d(size(imgSet{initIdx{i}})));
        imwrite(warpedImg,fullfile(finalTgtDir,sprintf(formatStrings{j},setIDs(i))));
        
        %Write small scale gif to disk
	warpedSmallImg = imresize(warpedImg,smallImgDownscaleFactor);
        [imind,cm] = rgb2ind(warpedSmallImg,256);
        gifFileName = fullfile(finalTgtDir,'mergedChannels.gif');
        if j == usedLayers(1)
            imwrite(imind,cm,gifFileName,'gif','Loopcount',inf,'DelayTime',0.5); 
        else 
            imwrite(imind,cm,gifFileName,'gif','WriteMode','append'); 
        end 

	%Write also them separately in a small folder
	finalTgtDirSmall = fullfile(finalFolderSmall,inputFolders{j}); %#ok<PFBNS> small
	if ~isfolder(finalTgtDirSmall), mkdir(finalTgtDirSmall); end
        imwrite(warpedSmallImg,fullfile(finalTgtDirSmall,sprintf(formatStrings{j},setIDs(i))));
	
    end
    fprintf('Set %d DONE.\n',setIDs(i));
end
