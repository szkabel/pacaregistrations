%Run it all

%% INPUTS

%This variable indicates whether to make calculations or just set the
%necessary variables

allTimeMeasure = tic();

setRegistrationPipeParameters;

%% Preprocessing
% And set some essential variables for later usage

fullInputFolders = cell(nofLayers,1);

for j=1:nofLayers
    fullInputFolders{j} = fullfile(inputBase,inputFolders{j});
end

if ~isfolder(targetDir), mkdir(targetDir); end

roughTgtDir = fullfile(targetDir,'RoughRegistered');

if ~isfolder(roughTgtDir), mkdir(roughTgtDir); end

N = length(setIDs);

ilastikFolder = fullfile(targetDir,'ilastik');
ilastikResDir = fullfile(ilastikFolder,'predictedImgs');
ilastikTmpDir = fullfile(ilastikFolder,'rescaledImgs');
ilastikTrainDir = fullfile(ilastikFolder,'trainImgs');

deconvolveOrigdDir = fullfile(targetDir,'DeconvolvedOrig');

roughRescaleFactor = 0.1;

filePath = strsplit(fileparts(mfilename('fullpath')),filesep);
%Line to be run if run with F9
%filePath = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\src\Matlab\Registration';
pyRegLoc = fullfile(strjoin(filePath(1:end-2),filesep),'Python');

usedLayers = unique(nodeList);

%% PIPELINE

parpool(nofThreads,'IdleTimeout',240);

rng(20190923);

% 1) First get Color Deconvolution Parameters
if exist(fullfile(targetDir,'colorDeconvMatrices.mat'),'file')
    load(fullfile(targetDir,'colorDeconvMatrices.mat'),'colorDeconvMatrices');
elseif exist(fullfile(targetDir,'singleStainSamples.mat'),'file')
    load(fullfile(targetDir,'singleStainSamples.mat'),'myStain');    
    colorDeconvMatrices = makeStains(myStain,nofLayers);
else    
    colorDeconvMatrices = estimateColorDeconvMatrix(nofLayers,setIDs,inputBase,inputFolders,targetDir,formatStrings);    
end

% 2) a) Run the color deconvolution on the original images (the blue deconvolution may be useful for registration)
%    b) Run Ilastik projects if the separation is done by that
if isnumeric(chToUseForRoughReg)    
    colorDeconvolveOrigSet;
    pyInputBase = fullfile(targetDir,'DeconvolvedOrig');    
elseif strcmp(chToUseForRoughReg,'ilastik')
    runIlastik;
    pyInputBase = ilastikResDir;
end

% 3) Run a preliminary rough registration
if exist(fullfile(targetDir,'roughRegistered_MetaData.mat'),'file')
    load(fullfile(targetDir,'roughRegistered_MetaData.mat'),'regTransform','regGraphs');
else
    runRoughRegistration;
end
 
% 4) Run smooth registration from python 
formatStringsPython = cellfun(@strrep,formatStrings,repmat({'%02d'},nofLayers,1),repmat({'{:02d}'},nofLayers,1),'UniformOutput',false);
% {
system([prePythonCall '; '...
    pythonLocation ' ' fullfile(pyRegLoc,'registerSmooth.py') ' '... 
    num2str(nofLayers) ' '...
    inputBase ' '...
    num2str(chToUseForRoughReg) ' '...
    targetDir ' '...
    num2str(nofThreads) ' '...
    strjoin(inputFolders,' ') ' '...
    strjoin(formatStringsPython,' ') ...
    ],'-echo');
% }

runCombinedRegistration;

% Color deconvolution on the registered images
% NOTE: alternatively consider here to apply the same transform on the
% already deconvolved images
colorDeconvolveSet;

collocalizeSet;

toc(allTimeMeasure);
