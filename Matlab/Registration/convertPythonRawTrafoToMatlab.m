function [tform] = convertPythonRawTrafoToMatlab(A,imgSizeSrc,imgSizeTgt)

if nargin<3
    imgSizeTgt = imgSizeSrc;    
end

imgSizeSrc = imgSizeSrc(1:2);
imgSizeTgt = imgSizeTgt(1:2);
A = reshape(A,[2 3]);

B = [A; 0 0 1]; 

B_trans = B(1:2,3);
B_trans = [eye(2) B_trans; 0 0 1]';
B_rot = B([1 2],[1 2]);
B_rot = [B_rot [0;0]; 0 0 1];

t_neg = [1 0 0; 0 1 0; -(imgSizeTgt-1)/2 1];
t_pos = [1 0 0; 0 1 0; (imgSizeSrc-1)/2 1];

%G as geometrical transform 
G = t_neg*B_rot*B_trans*t_pos;
Ginv = inv(G);
Ginv_swapped = Ginv([2 1 3],[2 1 3]); %Corrigate for python difference
Ginv_swapped(:,3) = [0;0;1];

tform = affine2d(Ginv_swapped);

end

