% To be run after plotBoxPlot (which requires setRegistartionPipeParameters
% and also fullPipe's first part)

%Create global coverage information based on ilastik transformed images
%(foreground) and color deconvoled values

bigFinalIlastikDir = fullfile(targetDir,'FinalIlastikRegistrationsBig');
deconvolvedDir = fullfile(targetDir,'Deconvolved');

stainChannel = 1; %Burnt in channel info modify in case of different color deconvolutions

k = 1;
refSampleProps = cell(1,length(pairwisePairs{2}));
refSignalOverSample = cell(1,length(pairwisePairs{2}));
refNormalizedSignalOverSample = cell(1,length(pairwisePairs{2}));
refPosSignalOverSample = cell(1,length(pairwisePairs{2}));
refSampleNames = cell(1,length(pairwisePairs{2}));
intSampleProps = cell(1,length(pairwisePairs{1}));
intSignalOverSample = cell(1,length(pairwisePairs{1}));
intNormalizedSignalOverSample = cell(1,length(pairwisePairs{1}));
intPosSignalOverSample = cell(1,length(pairwisePairs{1}));
intSampleNames = cell(1,length(pairwisePairs{1}));
for ii = fieldIDs(logRowIdx)'
    currBigImgDir = fullfile(bigFinalIlastikDir,num2str(ii,'%02d'));
    currDeconvDir = fullfile(deconvolvedDir,num2str(ii,'%02d'));
            
    for j=1:length(pairwisePairs{2})
        stainIdx = pairwisePairs{2}(j);
        stainName = stainNames{stainIdx};      
        [~,fileNameExex,currExt] = fileparts(sprintf(formatStrings{stainIdx},ii));
        currBigDeconvImg = imread(fullfile(currDeconvDir,[fileNameExex '_ch' num2str(stainChannel,'%02d') currExt]));
        currBigIlastikImg = imread(fullfile(currBigImgDir,sprintf(formatStrings{stainIdx},ii)));
        sampleBool = currBigIlastikImg > 128;
        refSampleProps{j}(k) = sum(sum(currBigDeconvImg(sampleBool) > stainThreshValues(stainIdx)))/sum(sum(sampleBool)); %signal coverage of the sample
        refSignalOverSample{j}(k) = sum(sum(currBigDeconvImg(sampleBool))); % sum of signal over sample
        refNormalizedSignalOverSample{j}(k) = sum(sum(currBigDeconvImg(sampleBool)))/sum(sum(sampleBool)); % normalized sum of signal over sample
        refPosSignalOverSample{j}(k) = sum(sum(currBigDeconvImg(currBigDeconvImg > stainThreshValues(stainIdx) & sampleBool))); % sum of positive signal over sample
        refSampleNames{j}{k} = stainName;
    end

    
    for i=1:length(pairwisePairs{1})
        stainIdx = pairwisePairs{1}(i);
        stainName = stainNames{stainIdx};
        [~,fileNameExex,currExt] = fileparts(sprintf(formatStrings{stainIdx},ii));        
        currBigDeconvImg = imread(fullfile(currDeconvDir,[fileNameExex '_ch' num2str(stainChannel,'%02d') currExt]));
        currBigIlastikImg = imread(fullfile(currBigImgDir,sprintf(formatStrings{stainIdx},ii)));
        sampleBool = currBigIlastikImg > 128;
        intSampleProps{i}(k) = sum(sum(currBigDeconvImg(sampleBool) > stainThreshValues(stainIdx)))/sum(sum(sampleBool)); %signal coverage of the sample
        intSignalOverSample{i}(k) = sum(sum(currBigDeconvImg(sampleBool))); % sum of signal over sample
        intNormalizedSignalOverSample{i}(k) = sum(sum(currBigDeconvImg(sampleBool)))/sum(sum(sampleBool)); % normalized sum of signal over sample
        intPosSignalOverSample{i}(k) = sum(sum(currBigDeconvImg(currBigDeconvImg > stainThreshValues(stainIdx) & sampleBool))); % sum of positive signal over sample
        intSampleNames{i}{k} = stainName;        
    end
    
    fprintf('%d DONE\n',k);
    k = k + 1;
end

df2 = fopen(fullfile(dataPath,'globSampleCoverage_df.csv'),'w');
for i=1:length(refSampleProps)
    for j=1:length(refSampleProps{i})
        fprintf(df2,'%f,%s,%d\n',refSampleProps{i}(j),refSampleNames{i}{j},idxOfInterest(j));
    end
end
for i=1:length(intSampleProps)
    for j=1:length(intSampleProps{i})
        fprintf(df2,'%f,%s,%d\n',intSampleProps{i}(j),intSampleNames{i}{j},idxOfInterest(j));
    end
end
fclose(df2);

df3 = fopen(fullfile(dataPath,'globSignalOverSample_df.csv'),'w');
for i=1:length(refSampleProps)
    for j=1:length(refSampleProps{i})
        fprintf(df3,'%f,%s,%d\n',refSignalOverSample{i}(j),refSampleNames{i}{j},idxOfInterest(j));
    end
end
for i=1:length(intSampleProps)
    for j=1:length(intSampleProps{i})
        fprintf(df3,'%f,%s,%d\n',intSignalOverSample{i}(j),intSampleNames{i}{j},idxOfInterest(j));
    end
end
fclose(df3);

df4 = fopen(fullfile(dataPath,'globNormSignalOverSample_df.csv'),'w');
for i=1:length(refSampleProps)
    for j=1:length(refSampleProps{i})
        fprintf(df4,'%f,%s,%d\n',refNormalizedSignalOverSample{i}(j),refSampleNames{i}{j},idxOfInterest(j));
    end
end
for i=1:length(intSampleProps)
    for j=1:length(intSampleProps{i})
        fprintf(df4,'%f,%s,%d\n',intNormalizedSignalOverSample{i}(j),intSampleNames{i}{j},idxOfInterest(j));
    end
end
fclose(df4);

df5 = fopen(fullfile(dataPath,'globPosSignalOverSample_df.csv'),'w');
for i=1:length(refSampleProps)
    for j=1:length(refSampleProps{i})
        fprintf(df5,'%f,%s,%d\n',refPosSignalOverSample{i}(j),refSampleNames{i}{j},idxOfInterest(j));
    end
end
for i=1:length(intSampleProps)
    for j=1:length(intSampleProps{i})
        fprintf(df5,'%f,%s,%d\n',intPosSignalOverSample{i}(j),intSampleNames{i}{j},idxOfInterest(j));
    end
end
fclose(df5);


