function [blockSize,xVals,yVals] = quickAutoCorrFWHM(img)
    
maxV = sum(sum(img.*img));
%NOTE: this is symmetric for sure.

%Positive in x
i = round(size(img,2)/2);
tform = affine2d([1 0 0; 0 1 0; i 0 1]);
imgT = imwarp(img,tform,'OutputView',imref2d(size(img)));
possMinV = sum(sum(imgT.*img));

xVals = zeros(1,floor(size(img,2)/2));
i = 1;
currV = maxV;
while i<size(img,2)/2 && currV > (maxV+possMinV)/2
    tform = affine2d([1 0 0; 0 1 0; i 0 1]);
    imgT = imwarp(img,tform,'OutputView',imref2d(size(img)));
    currV = sum(sum(imgT.*img));
    xVals(i) = currV;
    i = i + 1;
end
xPos = i;

%Positive in y

i = round(size(img,1)/2);
tform = affine2d([1 0 0; 0 1 0; 0 i 1]);
imgT = imwarp(img,tform,'OutputView',imref2d(size(img)));
possMinV = sum(sum(imgT.*img));

yVals = zeros(1,floor(size(img,1)/2));
i = 1;
currV = maxV;
while i<size(img,1)/2 && currV > (maxV+possMinV)/2
    tform = affine2d([1 0 0; 0 1 0; 0 i 1]);
    imgT = imwarp(img,tform,'OutputView',imref2d(size(img)));
    currV = sum(sum(imgT.*img));
    yVals(i) = currV;
    i = i + 1;
end
yPos = i;

blockSize = min(xPos,yPos);

end

