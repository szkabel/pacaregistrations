%script to reduce and make images grayscale from a folder

srcDir = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\190219_Samples\TMA4B_DHCR7_PC21';
tgtDir = 'D:\Abel\SZBK\Projects\NatasaRegistration\JuhoImgs';
imgExt = 'tif';

d = dir(fullfile(srcDir,['*.' imgExt ]));

for i=1:10%numel(d)
    img = imread(fullfile(srcDir,d(i).name));
    [~,filexex,~] = fileparts(d(i).name);
    imgG = rgb2gray(img);
    imgG_red = imresize(imgG,0.1);
    imwrite(imgG_red,fullfile(tgtDir,[filexex '_red_gs.' imgExt]));
end