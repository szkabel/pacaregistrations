%customImgSet = setdiff(1:62);
customImgSet = idxOfInterest;

load(fullfile(targetDir,'smoothRegistered_MetaData.mat'),'finalTransforms','initIdx');
ilastikPredFinDir = fullfile(targetDir,'FinalIlastikRegistrationsBig');
compositeOutDir = fullfile(targetDir,'FinalIlastikComposite');
deconvolvedDir = fullfile(targetDir,'Deconvolved'); %If fullpipe was run this should be there already. Needed to get the actual output dimensions

compositeUse = [2 3 9]; %should be of length 3 to keep it simple
if ~exist(ilastikPredFinDir,'dir'), mkdir(ilastikPredFinDir); end
if ~exist(compositeOutDir,'dir'), mkdir(compositeOutDir); end
composites = cell(length(customImgSet),1);

for i=1:length(customImgSet)
    
    currDeconvDir = fullfile(deconvolvedDir,num2str(customImgSet(i),'%02d'));
    tmpOutDir = fullfile(ilastikPredFinDir,num2str(customImgSet(i),'%02d'));
    if ~isfolder(tmpOutDir), mkdir(tmpOutDir); end
    origI = find(setIDs == customImgSet(i),1);
    %refImg = imread(fullfile(ilastikResDir,inputFolders{j},sprintf(formatStrings{j},initIdx{origI})));        
    for j=usedLayers'
        currImg = imread(fullfile(ilastikResDir,inputFolders{j},sprintf(formatStrings{j},customImgSet(i))));
        
        [~,fileNameExex,currExt] = fileparts(sprintf(formatStrings{j},customImgSet(i)));
        stainChannel = 1; % In this case this is arbitrary (all the images must be in the same size)
        currOutSpaceInfo = imfinfo(fullfile(currDeconvDir,[fileNameExex '_ch' num2str(stainChannel,'%02d') currExt]));
        
        downScaleM = [roughRescaleFactor 0 0; 0 roughRescaleFactor 0; 0 0 1];
        upScaleM = [1/roughRescaleFactor 0 0; 0 1/roughRescaleFactor 0; 0 0 1];
        warpedImg = imwarp(currImg,affine2d(upScaleM*finalTransforms{origI}.tforms{j}.T),'OutputView',imref2d([currOutSpaceInfo.Height currOutSpaceInfo.Width]));
        imwrite(warpedImg,fullfile(tmpOutDir,sprintf(formatStrings{j},customImgSet(i))));        
        %composites{i}{j} = warpedImg;        
    end
    
    %myRGB = cell2mat(reshape(composites{i}(compositeUse),1,1,3));
    %imwrite(myRGB,fullfile(compositeOutDir,sprintf('Composite_of_%d_%d_%d_Spot_%02d.png',compositeUse,customImgSet(i))));
                
    fprintf('Set %d DONE.\n',setIDs(i));
    
end
