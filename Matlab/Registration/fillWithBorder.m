function [rotatedImg] = fillWithBorder(origImg,rotatedImg)
%Fills the black (default filling after imwarp for empty regions) areas
%with random pixels from the border of the original image

nonEmptyIdx = find(~any(rotatedImg,3));
[neR, neC] = ind2sub([size(rotatedImg,1),size(rotatedImg,2)],nonEmptyIdx);

for i=1:length(nonEmptyIdx)
    if randi(2) == 1
            row = randi([0 1]);
            row = (size(origImg,1)-1)*row+1;
            col = randi(size(origImg,2));
        else
            col = randi([0 1]);
            col = (size(origImg,2)-1)*col+1;
            row = randi(size(origImg,1));
    end

    rotatedImg(neR(i),neC(i),:) = origImg(row,col,:);        
end

end
