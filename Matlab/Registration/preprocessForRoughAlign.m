function [preparedImg,img_sat] = preprocessForRoughAlign(img)

padSize = [64 64];
scaledSize = [256,256];
smoothingSigma = 2;
S1 = strel('disk',8,0);
S2 = strel('disk',8,0);

img_hsv = rgb2hsv(img);
img_sat = img_hsv(:,:,2);

img_pipe = imresize(img_sat,scaledSize);
img_pipe = padarray(img_pipe,padSize,'both');

img_pipe = imgaussfilt(img_pipe,smoothingSigma);

img_pipe = imbinarize(img_pipe);

img_pipe = imclose(imopen(img_pipe,S1),S2);

preparedImg = double(img_pipe(padSize(1)+1:end-padSize(1),padSize(2)+1:end-padSize(2)));

end