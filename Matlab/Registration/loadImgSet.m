function [imgSet] = loadImgSet(imgDirs,formatStrings,setID)

nofLayers = length(imgDirs);
imgSet = cell(1,nofLayers);

for i=1:nofLayers
    imgSet{i} = imread(fullfile(imgDirs{i},sprintf(formatStrings{i},setID)));
end

end

