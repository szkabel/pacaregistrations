%Custom script for the AIFORIA Project

srcPathHERO = 'E:\Abelin_analysoitavat\Aiforia\HERO_tiffs';
srcPathSHG = 'E:\Abelin_analysoitavat\Aiforia\SHG_Stitch_Crop_Flip';
workDir = 'E:\Abelin_analysoitavat\Aiforia\WorkDir';
outDir = 'E:\Abelin_analysoitavat\Aiforia\Registered';

amdLocation = 'Z:\Projects\Juho\PaCa\pancreaticcodes\python\register_example3.py';
pythonLocation = 'C:\Hyapp\Anaconda3\python.exe';

downScaleFactor = 10;
chToUseFromHERO = 2;
imgIdxToRun = [1 2 4];

if ~isdir(outDir), mkdir(outDir); end
%Go through all files in the input folder
parfor ii = 1:length(imgIdxToRun)
    i = imgIdxToRun(ii);
    tic
    currHeroName = ['A' num2str(i,'%02d') '.ome.tif'];
    currEpiName  = ['A' num2str(i,'%02d') '_ch00.tif'];
    currFName    = ['A' num2str(i,'%02d') '_ch01.tif'];
    fprintf('Reading image: %s ...\n',fullfile(srcPathHERO,currHeroName));
    heroImg   = imread(fullfile(srcPathHERO,currHeroName));
    fprintf('Reading image: %s ...\n',fullfile(srcPathSHG,currEpiName));
    shgEpiImg = imread(fullfile(srcPathSHG,currEpiName));
    fprintf('Reading image: %s ...\n',fullfile(srcPathSHG,currFName));
    shgFImg   = imread(fullfile(srcPathSHG,currFName));
    
    currWD = fullfile(workDir,['A' num2str(i,'%02d')]);
    if ~isdir(currWD), mkdir(currWD); end
    
    fprintf('Writing rescaled image: %s ...\n',fullfile(currWD,currHeroName));
    imwrite(imresize(255-heroImg(:,:,chToUseFromHERO),1/downScaleFactor),fullfile(currWD,currHeroName));
    fprintf('Writing rescaled image: %s ...\n',fullfile(currWD,currEpiName));
    imwrite(imresize(shgEpiImg(:,:),1/downScaleFactor),fullfile(currWD,currEpiName));
    
    heroImgSize = size(heroImg);
    heroImg = []; %Clear it up to save memory
    
    %Call registration
    fprintf('Python is starting up the registration...\n');
    system([pythonLocation ' '...
        amdLocation ' ' ...        
        fullfile(currWD,currHeroName) ' ' ... % This is the ref
        fullfile(currWD,currEpiName) ' ' ... %This is the flo
        currWD],'-echo');
    
    %Read and transform its outputs
    outputFile = fullfile(currWD,'transform_parameters.txt');
    registrationMatrix = load(outputFile);    
    smallTform = convertPythonRawTrafoToMatlab(registrationMatrix,size(shgEpiImg)/downScaleFactor,heroImgSize/downScaleFactor);
    smallTformM = smallTform.T;
    
    %Create the transform for the big matrix
    downTrafoM = [1/downScaleFactor 0 0; 0 1/downScaleFactor 0; 0 0 1];
    upTrafoM = [downScaleFactor 0 0; 0 downScaleFactor 0; 0 0 1];
    
    tformBig = affine2d(downTrafoM*smallTformM*upTrafoM);
    
    %Transform the big images
    epiTfmd = imwarp(shgEpiImg,tformBig,'OutputView',imref2d(heroImgSize));
    fTfmd = imwarp(shgFImg,tformBig,'OutputView',imref2d(heroImgSize));
    
    %Flush them to disk
    imwrite(epiTfmd,fullfile(outDir,currEpiName));
    imwrite(fTfmd,fullfile(outDir,currFName));
        fprintf('****************************************\n\n SET %d/%d (set %d) DONE\nElapsed time: %f sec\n\n****************************************\n',ii,length(imgIdxToRun),i,toc);
end