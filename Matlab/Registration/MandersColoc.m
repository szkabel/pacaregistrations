function [M1,M2] = MandersColoc(imgR,imgG,threshLevelR,threshLevelG,both)
%Simple function to implement Mander's colocalization method based on his
%paper

%M1 is the proportion of Red pixels that overlap with Green objects
%M2 is the proportion of Green pixels that overlap with Red objects
%'objects' can be defined by the image itself and a thresholdLevel
%
%Therefore the input 2 images (imgR and imgG) should be dark-backgrounded,
%signal should be bright on them

%both: boolean identifying whether to apply the threshold also on the
%input image. NOTE: change on 2020.06.03. 'both' means that the input signal
%is also thresolded (i.e. considered 0 outside the positive area identified
%by the threshold). In this case M1 is the proportion of the *positive* Red
%intensities that overlap with positive green.

if nargin<5, both = false; end

if both
    sumR = sum(sum(  imgR( imgR>threshLevelR)  )); %NOTE: it is unlikely that the sum(sum( is actually needed, but it is possible
    sumG = sum(sum(  imgG( imgG>threshLevelG)  ));
    M1 = sum(sum(imgR(imgG>threshLevelG & imgR>threshLevelR)))/sumR; 
    M2 = sum(sum(imgG(imgR>threshLevelR & imgG>threshLevelG)))/sumG;    
else
    sumR = sum(sum(imgR));
    sumG = sum(sum(imgG));
    M1 = sum(sum(imgR(imgG>threshLevelG)))/sumR;
    M2 = sum(sum(imgG(imgR>threshLevelR)))/sumG;
end

end