%Custom script for the AIFORIA Project

%% Parameter settings

dL = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\SHG_HE';
srcPathHERO = [dL '\HE\renamed'];
srcPathSHG = [dL '\SHG\renamed'];
workDir = [dL '\wrkdir'];
outDir = [dL '\result'];

HE_Pattern  = 'PC51_HE_#%d_%02d-%d_%d_ses1_v1.tif';
SHG_Pattern = 'PC51_SHG_#%d_%02d-%d_%d_ses%d_v%d.lif';
outExt = 'tif';

downScaleFactor = 10;
chToUseFromHERO = 1;

imgIdxToRun = [...%8 1 3 1 1 1 ;...
               %7 5 3 1 2 1 ;...
               %8 3 3 1 2 1 ;...
               8 4 3 1 1 1 ;...
               8 5 3 1 1 2 ;...
               8 6 3 1 1 1 ;...
               8 6 3 2 1 1 ;...
               ];
%An index referencing array that identifies which imgIdxs are needed for
%identifying the image
idxForHE  = logical([1 1 1 1 0 0]);
idxForSHG = logical([1 1 1 1 1 1]);

% The parameters below should form a pipeline for the SHG layers.
nofLayersInLif = 4;
layersForEstimation = [0  1  0  1;... %These are the weights for estimating the the stitching.
                       1 .5  1 .5];
layersForEstimation(1,:) = layersForEstimation(1,:) ./ sum(layersForEstimation(1,:));

layersForStitcing = 1:nofLayersInLif; %NOTE: we need to stitch all of those images that we would like to use for registration (and the weightsToUseForReg array must match up with this number).

weightsToUseForReg = [0  .5  .2  .3;... %First line is weighted average, Reg stands here for registration.
                      1  .5  .9  .5]; %Second line is gamma
                  
weightsToUseForReg(1,:) = weightsToUseForReg(1,:) ./ sum(weightsToUseForReg(1,:));

layersForFinalRegistration = 1:nofLayersInLif; %Which indices we would like to use for the final stitching. The indices here shoud refer to the positions in the layers for stitching output.

nofSkippedTilesInTheEnd = 1; %This is needed as the lif contains a final level with the stitched image which is not send for Cidre

%amdLocation = 'Z:\Projects\Juho\PaCa\pancreaticcodes\python\register_example3.py';
amdLocation = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\src\Python\register_example3.py';
%pythonLocation = 'C:\hyapp\Anaconda3-2019.3\python.exe';
pythonLocation = 'C:\Users\szkabel\Anaconda3\python.exe';

[~,wrk_Pattern,~] = fileparts(SHG_Pattern);
cidreExt = 'png';
wrkExt = 'tif'; % MUST BE TIF FOR CRAZY BIG (byte count > 2^32-1) IMAGES.

cidreWrkDir = fullfile(workDir,'CIDRE');
layerCidreWrkDirs = cell(nofLayersInLif,1);
for j=1:nofLayersInLif, layerCidreWrkDirs{j} = sprintf('Layer_%02d',j); end
cidreDest = fullfile(cidreWrkDir,'destination');

nofImgs = size(imgIdxToRun,1); 
shgBigTiffBool = false(nofImgs,1);

%% PROCESSING

%% Preparing CIDRE -> 
%{
% NOTE After each run the corrected images should be re-loaded into the
% lif array, so that they can be registered later
for j=1:nofLayersInLif
    if ~exist(fullfile(cidreWrkDir,layerCidreWrkDirs{j}),'dir'), mkdir(fullfile(cidreWrkDir,layerCidreWrkDirs{j})); end    
end
if ~exist(cidreWrkDir,'dir'), mkdir(cidreWrkDir); end

for ii=1:nofImgs
    fprintf('Preparing for CIDRE %d/%d...\n',ii,nofImgs);
    i = imgIdxToRun(ii,idxForSHG);
    currSHGName  = sprintf(SHG_Pattern,i);
    shgBF = bfopen2(fullfile(srcPathSHG,currSHGName),[0,nofSkippedTilesInTheEnd]);
    shgBFImgs = shgBF(:,1);
    for j=1:nofLayersInLif
        parfor k=1:size(shgBF,1)
            currFileName = sprintf([ SHG_Pattern ,'_Tile%03d.%s'],i,k,cidreExt);
            imwrite(shgBFImgs{k}{j,1},fullfile(cidreWrkDir,layerCidreWrkDirs{j},currFileName)); %#ok<PFBNS>
        end
    end    
end

%% Run CIDRE
fprintf('Running CIDRE...\n');
if ~exist(cidreDest,'dir'), mkdir(cidreDest); end
parfor j=1:nofLayersInLif    
    cidre(fullfile(cidreWrkDir,layerCidreWrkDirs{j},['*.' cidreExt]),'destination',fullfile(cidreDest,layerCidreWrkDirs{j}));
end

%% Reload CIDRE corrected
% back to lif structure and save it out as stitched img

%Replace the image sets with the CIDRE version
for ii = 1:nofImgs
    i = imgIdxToRun(ii,idxForSHG);
    currSHGName  = sprintf(SHG_Pattern,i);
    shgBF = bfopen2(fullfile(srcPathSHG,currSHGName),[0,nofSkippedTilesInTheEnd]);
    shgBFImgs = shgBF(:,1);
    for j=1:nofLayersInLif
        fprintf('Reloading CIDRE. Image %d/%d - Layer %d/%d...\n',ii,nofImgs,j,nofLayersInLif);
        for k=1:size(shgBF,1)-nofSkippedTilesInTheEnd
            currFileName = sprintf([ SHG_Pattern ,'_Tile%03d.%s'],i,k,cidreExt);
            reloadImg = imread(fullfile(cidreDest,layerCidreWrkDirs{j},currFileName)); %#ok<PFBNS>
            shgBFImgs{k}{j,1} = reloadImg;
        end
    end    
    shgBF(:,1) = shgBFImgs;
    clear('shgBFImgs','reloadImg'); %For memory save
    fprintf('Converting Lif to image structure...\n');
    [imgStruct,pixelSizes,nRow,nCol] = convertLifArrayToImageStruct(shgBF);
    fprintf('DONE.\n');
    
    % For TMP usage
    % sL4Estim = layersForEstimation; sL4Stitch = layersForStitcing;        
    stitch2Dgrid(imgStruct,pixelSizes,layersForEstimation,layersForStitcing,nRow,nCol,'writeFile',true,'filePattern',fullfile(cidreWrkDir,'tmp_ch%02d.png'));
    
    stitchedImg = cell(nofLayersInLif);
    for j=1:nofLayersInLif
        stitchedImg{j} = imread(fullfile(cidreWrkDir,sprintf('tmp_ch%02d.png',j)));
    end
    imgData = cat(3,stitchedImg{:});
    W = whos('imgData');
    imgName = fullfile(cidreWrkDir,sprintf([wrk_Pattern '.' wrkExt],i));
    %Write big Tiff 
    if W.bytes > 2^32-1
        saveastiff(imgData,imgName);
        shgBigTiffBool(ii) = true;
    else
        imwrite(imgData,imgName);
    end
end
%}
if ~isdir(outDir), mkdir(outDir); end %#ok<ISDIR> May need to run on older versions

%% Run the registration

fprintf('*******************************\n\n\t\t\tRUNNING REGISTRATION\n\n*******************************\n')

%Go through all files in the input folder
for ii = 1:nofImgs
    shg_i = imgIdxToRun(ii,idxForSHG);
    he_i = imgIdxToRun(ii,idxForHE);
    tic
    currHeroName = sprintf(HE_Pattern,he_i);
    currSHGName  = sprintf([wrk_Pattern '.' wrkExt],shg_i);
    fprintf('Reading image: %s ...\n',fullfile(srcPathHERO,currHeroName));
    %We changed the czi to tif
    heroImg = imread(fullfile(srcPathHERO,currHeroName));
    fprintf('Reading image: %s ...\n',fullfile(cidreWrkDir,currSHGName));
    %Change the name now to tif (it is easier for everyone)
    if shgBigTiffBool(ii)
        shgImg = loadtiff(fullfile(cidreWrkDir,currSHGName));
    else
        shgImg = imread(fullfile(cidreWrkDir,currSHGName));
    end
        
    fprintf('Reading imgs is DONE\n');                
    
    useForRegImg = averageLayers(shgImg,downScaleFactor,weightsToUseForReg);    
      
    currWD = fullfile(workDir,sprintf(wrk_Pattern,shg_i));
    if ~isdir(currWD), mkdir(currWD); end %#ok<ISDIR> Might need to be run on older matlabs
    
    fprintf('Writing rescaled image: %s ...\n',fullfile(currWD,currHeroName));
    imwrite(imresize(255-heroImg(:,:,chToUseFromHERO),1/downScaleFactor),fullfile(currWD,currHeroName));
    fprintf('Writing rescaled image: %s ...\n',fullfile(currWD,currSHGName));
    imwrite(useForRegImg,fullfile(currWD,currSHGName));
    
    heroImgSize = size(heroImg);
    heroImg = []; %Clear it up to save memory
    
    %Call registration
    fprintf('Python is starting up the registration...\n');
    system([pythonLocation ' '...
        amdLocation ' ' ...        
        fullfile(currWD,currHeroName) ' ' ... % This is the ref
        fullfile(currWD,currSHGName) ' ' ... %This is the flo
        currWD],'-echo');
    
    %Read and transform its outputs
    outputFile = fullfile(currWD,'transform_parameters.txt');
    registrationMatrix = load(outputFile);    
    smallTform = convertPythonRawTrafoToMatlab(registrationMatrix,[size(shgImg,1) size(shgImg,2)]/downScaleFactor,heroImgSize/downScaleFactor);
    smallTformM = smallTform.T;
    
    %Create the transform for the big matrix
    downTrafoM = [1/downScaleFactor 0 0; 0 1/downScaleFactor 0; 0 0 1];
    upTrafoM = [downScaleFactor 0 0; 0 downScaleFactor 0; 0 0 1];
    
    tformBig = affine2d(downTrafoM*smallTformM*upTrafoM);
    
    %Transform the big images and flush them out
    [~,SHG_out_Pattern,~] = fileparts(SHG_Pattern);
    SHG_out_Pattern = [SHG_out_Pattern '_ch%02d'  '.' outExt]; %#ok<AGROW>
    for j=1:length(layersForFinalRegistration)
        tfmd = imwarp(shgImg(:,:,layersForFinalRegistration(j)),tformBig,'OutputView',imref2d(heroImgSize));
        imwrite(tfmd,fullfile(outDir,sprintf(SHG_out_Pattern,shg_i,layersForStitcing(layersForFinalRegistration(j)))));
    end    
            
    fprintf(['****************************************\n\n SET %d/%d (set ' SHG_Pattern ') DONE\nElapsed time: %f sec\n\n****************************************\n'],ii,nofImgs,shg_i,toc);
end