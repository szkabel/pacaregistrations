%Script to plot boxplots in a more reasonable manner

%dataPath = 'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\TMAs\fullRun\CollocResults6';
%dataPath = 'G:\Abel\Research\Projects\Juho\PancreaticCancer\TMAs\reducedRun\CollocResults';
dataPath = 'L:\ltdk_ikonen\Abel\Juho\PaCa\TMAs\reducedRun\CollocResults';

myData = load(fullfile(dataPath,'bareData.txt'));
myVarNames = readtable(fullfile(dataPath,'varNameList.txt'),'ReadVariableNames',false);
myVarNames = myVarNames.Variables;

ignoreList = [1 2 3 4 6 7 9 24 27 32 38 41 43 48 57]; % 1-4 leave out, as those are control spots. The others are not of good enough quality
pairwisePairs = {...
    [3 7];...
    [2 9];...
    };
margin = 5;
dist = 2;
bWidth = 5;

fieldIdIdx = find(strcmp(myVarNames,'fieldID'));
fieldIDs = myData(:,fieldIdIdx);
idxOfInterest = setdiff(fieldIDs,ignoreList);
logRowIdx = ismember(fieldIDs,idxOfInterest); % logical row idx

stainThreshValues = ...
    [45;... %ACLS3
     15;...  %CK7
     20;... %DHCR7
     45;... %DHCR24
     47;... %FASN (not really reliable)
     21;... %Langerin
     55;... %LDLR
     23;... %MUC
     29;... %SMA
     ];

stainNames = ...
    {'ACLS3',...
     'CK7',...
     'DHCR7',...
     'DHCR24',...
     'FASN',...
     'Langerin',...
     'LDLR',...
     'MUC',...
     'SMA',...
     };


nofGroups = prod(cellfun(@length,pairwisePairs));

%+1 for the CK7 vs SMA
boxData = cell(nofGroups+sum(cellfun(@length,pairwisePairs))+1,6);

refGlobProps = cell(1,length(pairwisePairs{2}));
for j=1:length(pairwisePairs{2})
    stainName = stainNames{pairwisePairs{2}(j)};
    currGroupIdx = find(ismember(myVarNames,...
            ['globalPropOf_' stainName]),1,'first');
    refGlobProps{j} = myData(logRowIdx,currGroupIdx);
end

intGlobProps = cell(1,length(pairwisePairs{1}));
for i=1:length(pairwisePairs{1})
    stainName = stainNames{pairwisePairs{1}(i)};
    currGroupIdx = find(ismember(myVarNames,...
            ['globalPropOf_' stainName]),1,'first');
    intGlobProps{i} = myData(logRowIdx,currGroupIdx);
end

k = 1;
for i=1:length(pairwisePairs{1})
    for j=1:length(pairwisePairs{2})        
        redStainName = stainNames{pairwisePairs{1}(i)};
        greenStainName = stainNames{pairwisePairs{2}(j)};        
        currGroupIdx = find(ismember(myVarNames,...
            ['propOf_thresholded_' redStainName   '(Manual_' num2str(stainThreshValues(pairwisePairs{1}(i)))   ')_over_thresholded_' greenStainName '_(Manual_' num2str(stainThreshValues(pairwisePairs{2}(j))) ')']));                
        boxData{k,1} = myData(logRowIdx,currGroupIdx);
        boxData{k,2} = repmat({[ redStainName ' vs. ' greenStainName ]},sum(logRowIdx),1);        
        boxData{k,3} = repmat({greenStainName},sum(logRowIdx),1);
        boxData{k,4} = myData(logRowIdx,currGroupIdx+1);
        boxData{k,5} = repmat({redStainName},sum(logRowIdx),1);
        boxData{k,6} = idxOfInterest;
        k = k + 1;
    end
end

Y = cell2mat(boxData(:,1));
X = margin+mod((1:nofGroups)-1,length(pairwisePairs{2}))*(bWidth+dist)+bWidth/2+dist + floor(((1:nofGroups)-1)/length(pairwisePairs{2}))*length(pairwisePairs{2})*(bWidth+dist+margin);
groupV = vertcat(boxData{:,2});
colV = vertcat(boxData{:,3});
% pvals = cellfun(@mean,boxData(:,4));
% pvalsAll = cell2mat(boxData(:,4));
% stainV = vertcat(boxData{:,5});

% colors = [...
%     .7 .3 .3;...
%     .3 .7 .3;...
%     .3 .3 .7;...
%     .7 .7 .3;...        
%     ];

colors = [...
    248,118,109;...
    124,174,0;...
    0,191,196;...
    199,124,255;...        
    ]./255;

figure;

subplot(2,2,1);
boxplot(Y,groupV,...
    'ColorGroup',colV,...
    'Colors',colors,...
    'Widths',bWidth,...
    'Positions',X,...
    'BoxStyle','outline',...    
    'Symbol','+',...
    'OutlierSize',4,...
    'Notch','off',...
    'LabelOrientation','inline'...    
    );

title('Collocalization of TMAs');

subplot(2,2,3);
boxplot(cell2mat(intGlobProps));
xticklabels(stainNames(pairwisePairs{1}'));
title('Global proportion of stains of interest');

subplot(2,2,2);
boxplot(cell2mat(refGlobProps),'Colors',colors);
xticklabels(stainNames(pairwisePairs{2}'));
title('Global proportion of stains of reference');


%extra lines:
% Global proportions
for i=1:length(pairwisePairs{1})
    boxData{k,1} = intGlobProps{i};
    currStainName = stainNames{pairwisePairs{1}(i)};
    boxData{k,2} = repmat({[ currStainName ' vs. glob' ]},sum(logRowIdx),1);
    boxData{k,3} = repmat({currStainName},sum(logRowIdx),1);
    boxData{k,4} = nan(sum(logRowIdx),1);
    boxData{k,5} = repmat({ 'global' },sum(logRowIdx),1);
    boxData{k,6} = idxOfInterest;
    k = k + 1;
end

for i=1:length(pairwisePairs{2})
    boxData{k,1} = refGlobProps{i};
    currStainName = stainNames{pairwisePairs{2}(i)};
    boxData{k,2} = repmat({[ currStainName ' vs. glob' ]},sum(logRowIdx),1);
    boxData{k,3} = repmat({currStainName},sum(logRowIdx),1);
    boxData{k,4} = nan(sum(logRowIdx),1);
    boxData{k,5} = repmat({ 'global' },sum(logRowIdx),1);
    boxData{k,6} = idxOfInterest;
    k = k + 1;
end

%CK7 vs SMA

redIdx = 2;
greenIdx = 9;
redStainName = stainNames{redIdx};
greenStainName = stainNames{greenIdx};        
currGroupIdx = find(ismember(myVarNames,...
    ['propOf_thresholded_' redStainName   '(Manual_' num2str(stainThreshValues(redIdx))   ')_over_thresholded_' greenStainName '_(Manual_' num2str(stainThreshValues(greenIdx)) ')']));                
boxData{k,1} = myData(logRowIdx,currGroupIdx);
boxData{k,2} = repmat({[ redStainName ' vs. ' greenStainName ]},sum(logRowIdx),1);        
boxData{k,3} = repmat({greenStainName},sum(logRowIdx),1);
boxData{k,4} = myData(logRowIdx,currGroupIdx+1);
boxData{k,5} = repmat({redStainName},sum(logRowIdx),1);
boxData{k,6} = idxOfInterest;
k = k + 1;

%Recalc things
Y = cell2mat(boxData(:,1));
X = margin+mod((1:nofGroups)-1,length(pairwisePairs{2}))*(bWidth+dist)+bWidth/2+dist + floor(((1:nofGroups)-1)/length(pairwisePairs{2}))*length(pairwisePairs{2})*(bWidth+dist+margin);
groupV = vertcat(boxData{:,2});
colV = vertcat(boxData{:,3});
pvals = cellfun(@mean,boxData(:,4));
pvalsAll = cell2mat(boxData(:,4));
stainV = vertcat(boxData{:,5});
fieldIDs = vertcat(boxData{:,6});


df = fopen(fullfile(dataPath,'filtered_df_check.csv'),'w');
fprintf(df,'StainProp,Case,ReferenceStain,p.value,StainOfInterest,fieldID\n');
for i=1:length(Y)
    fprintf(df,'%f,%s,%s,%f,%s,%d\n',Y(i),groupV{i},colV{i},pvalsAll(i),stainV{i},fieldIDs(i));
end
fclose(df);

