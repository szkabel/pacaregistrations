%Create table for colocalization analysis

deconvolvedDir = fullfile(targetDir,'Deconvolved');
postProcessedDir = fullfile(targetDir,'PostProcess');
if ~isfolder(postProcessedDir), mkdir(postProcessedDir); end

stainChannel = 1; %Burnt in channel info modify in case of different color deconvolutions

% Numbers for 50-99.99% cutoff
stainThreshValues = ...
    [45;... %ACLS3
     15;...  %CK7
     20;... %DHCR7
     45;... %DHCR24
     47;... %FASN (not really reliable)
     21;... %Langerin
     55;... %LDLR
     23;... %MUC
     29;... %SMA
     ];
 
%Calculate block sizes in advance for each channel
if ~exist(fullfile(targetDir,'channelBlockSizes.mat'),'file')
    % In case this is NOT always the first channel then this needs to be
    % changed (maybe even to an array if the channel of interest is not
    % constant among layers). The channel of interest is defined by the color
    % deconvolution parameters.
    channelOfInterests = 1;
    blockSizeData = cell(nofLayers,1);
    blockSizeExtraData = cell(nofLayers,1);
    fprintf('Calculating block sizes for Manders colloc\n');
    for ii=1:nofLayers   
        fprintf('Channel %d is in progress...\n',ii);    
        locID = cell(length(setIDs),1);
        locBS = cell(length(setIDs),1);
        locX = cell(length(setIDs),1);
        locY = cell(length(setIDs),1);
        parfor i=1:length(setIDs)
            currDecDir = fullfile(deconvolvedDir,num2str(setIDs(i),'%02d'));
            chFileName = sprintf(formatStrings{ii},setIDs(i)); %#ok<PFBNS> small array
            [~,fnexex,ext] = fileparts(chFileName);
            img = imread(fullfile(currDecDir,[fnexex '_ch' num2str(channelOfInterests,'%02d') ext]));
            imgThresh = img;
            imgThresh(img < stainThreshValues(ii) ) = 0; %#ok<PFBNS> small array
            [blockSizeLoc,xVals,yVals] = quickAutoCorrFWHM(imgThresh);
            locID{i} = setIDs(i);
            locBS{i} = blockSizeLoc;
            locX{i} = xVals;
            locY{i} = yVals;
        end
        blockSizeData{ii} = [locID locBS];
        blockSizeExtraData{ii} = [locX locY];
        fprintf('Channel %d is DONE.\n',ii);
        save(fullfile(targetDir,'channelBlockSizes.mat'),'blockSizeData','blockSizeExtraData');
    end
else
     load(fullfile(targetDir,'channelBlockSizes.mat'),'blockSizeData');
end

constBlockSizePerChannel = zeros(nofLayers,1);
for i=1:nofLayers
    constBlockSizePerChannel(i) = round(median(cell2mat(blockSizeData{i}(:,2))))*2; %Multipled by 2 as the paper suggest Full Width Half Maximum, and the quickAutoCorrFWHM only calculates the half length.
end

for ii=1:size(POI,1)
    %naming conventions are red and green for 1st and 2nd channel
    %This refers to the stain number (or layer, MUC, SMA etc.)
    redStain = POI(ii,1);
    redStainName = strsplitN(inputFolders{redStain},'_',2);
    redStainChannel = 1;
    redThreshCorrFactor = 1.75;
    gaussSigmRed = 5;

    greenStain = POI(ii,2);
    greenStainName = strsplitN(inputFolders{greenStain},'_',2);
    greenStainChannel = 1;
    greenThreshCorrFactor = 1.75;
    gaussSigmGreen = 5;

    strelSizeForTopHat = 40;

    %Table for the collocalization
    collocTable = cell(N);
    warning('off','MATLAB:table:RowsAddedExistingVars');
    
    %Get threshold values
    redT = mean(stainThreshValues(redStain,:));
    greenT = mean(stainThreshValues(greenStain,:));

    headerNames = {'ID';...
        ['propOf_'   redStainName '_over_' greenStainName];...
        'pValue';...
        ['propOf_' greenStainName '_over_'   redStainName];...
        'pValue';...
        ['propOf_'   redStainName '_over_thresholded_' greenStainName '_(Manual_' num2str(greenT) ')'];...
        'pValue';...
        ['propOf_' greenStainName '_over_thresholded_'   redStainName '_(Manual_' num2str(  redT) ')'];...
        'pValue';...
        ['propOf_thresholded_' redStainName   '(Manual_' num2str(redT)   ')_over_thresholded_' greenStainName '_(Manual_' num2str(greenT) ')'];...
        'pValue';...
        ['propOf_thresholded_' greenStainName '(Manual_' num2str(greenT) ')_over_thresholded_' redStainName   '_(Manual_' num2str(redT)   ')'];...
        'pValue';...
        'PearsonCorr';...
        'pValue';...
        'PearsonCorrBoth';...
        'pValue';...
        ['globalPropOf_' redStainName];...
        ['globalPropOf_' greenStainName];...
        'JaccardIndex';...
        'pValue';...
        };    
    
    fprintf('Co-localizing %s and %s...\n',redStainName,greenStainName);
    parfor i=1:length(setIDs)    
        currDecDir = fullfile(deconvolvedDir,num2str(setIDs(i),'%02d'));        

        redFileName = sprintf(formatStrings{redStain},setIDs(i)); %#ok<PFBNS>
        [~,fnexexR,extR] = fileparts(redFileName);

        greenFileName = sprintf(formatStrings{greenStain},setIDs(i));
        [~,fnexexG,extG] = fileparts(greenFileName);

        imgRed = imread(fullfile(currDecDir,[fnexexR '_ch' num2str(redStainChannel,'%02d') extR]));
        imgGreen = imread(fullfile(currDecDir,[fnexexG '_ch' num2str(greenStainChannel,'%02d') extG]));

        maxRedV = getMaxImgValFromClass(imgRed);
        maxGreenV = getMaxImgValFromClass(imgGreen);

        %redT_base = graythresh(imgRed)*redThreshCorrFactor;
        %redT = redT_base*maxRedV;        
        redT_base = redT / maxRedV;
        %greenT_base = graythresh(imgGreen)*greenThreshCorrFactor;
        %greenT =  greenT_base*maxGreenV;        
        greenT_base = greenT / maxGreenV;

        %imgRedTophat = imtophat(imgaussfilt(imgRed,gaussSigmRed),strel('disk',strelSizeForTopHat));
        %imgGreenTophat = imtophat(imgaussfilt(imgGreen,gaussSigmGreen),strel('disk',strelSizeForTopHat));

        %redTtop_base = graythresh(imgRedTophat)*redThreshCorrFactor;
        %redTtop = redTtop_base*maxRedV;
        %greenTtop_base = graythresh(imgGreenTophat)*greenThreshCorrFactor;
        %greenTtop = greenTtop_base*maxGreenV;

        [M1,M2] = MandersColoc(imgRed,imgGreen,0,0);
        [tM1,tM2] = MandersColoc(imgRed,imgGreen,redT,greenT);
        [tbM1,tbM2] = MandersColoc(imgRed,imgGreen,redT,greenT,true);
        corrV = corr2(imgRed,imgGreen);
        unionImg = imgRed > redT | imgGreen > greenT;
        C = corrcoef(double(imgRed(unionImg)),double(imgGreen(unionImg)));
        corrVBoth = C(1,2);
        intersectImg = imgRed > redT & imgGreen > greenT;
        jaccardIdx = sum(intersectImg(:))/sum(unionImg(:));
        
        blockSize = min(constBlockSizePerChannel(redStain),constBlockSizePerChannel(greenStain)); %#ok<PFBNS> It's not big
        
        nofIterations = 200;
        t = tic();
        [M1A,M2A,tM1A,tM2A,tbM1A,tbM2A,corrVA,corrVBothA,jaccardIdxA] = randomShuffleManders(imgRed,imgGreen,redT,greenT,nofIterations,blockSize);
        fprintf('The running time for random shuffle was %f\n',toc(t));
        p_M1 = 1 - ( sum(M1 > M1A)/nofIterations );
        p_M2 = 1 - ( sum(M2 > M2A)/nofIterations );
        p_tM1 = 1 - ( sum(tM1 > tM1A)/nofIterations );
        p_tM2 = 1 - ( sum(tM2 > tM2A)/nofIterations );
        p_tbM1 = 1 - ( sum(tbM1 > tbM1A)/nofIterations );
        p_tbM2 = 1 - ( sum(tbM2 > tbM2A)/nofIterations );
        p_corrV = 1 - ( sum(corrV > corrVA)/nofIterations );
        p_corrVBoth = 1 -  ( sum(  corrVBoth > corrVBothA )/nofIterations );
        p_jaccardIdx = 1 - ( sum( jaccardIdx > jaccardIdxA)/nofIterations );
                
        
        j = 1;
        collocTable{i} = cell(1,length(headerNames));
        collocTable{i}{j} = setIDs(i); j = j + 1;
        collocTable{i}{j} = M1; j = j + 1;
        collocTable{i}{j} = p_M1; j = j + 1;
        collocTable{i}{j} = M2; j = j + 1;
        collocTable{i}{j} = p_M2; j = j + 1;
        collocTable{i}{j} = tM1; j = j + 1;
        collocTable{i}{j} = p_tM1; j = j + 1;
        collocTable{i}{j} = tM2; j = j + 1;
        collocTable{i}{j} = p_tM2; j = j + 1;
        collocTable{i}{j} = tbM1; j = j + 1;
        collocTable{i}{j} = p_tbM1; j = j + 1;
        collocTable{i}{j} = tbM2; j = j + 1;
        collocTable{i}{j} = p_tbM2; j = j + 1;        
        collocTable{i}{j} = corrV; j = j + 1;       
        collocTable{i}{j} = p_corrV; j = j + 1;
        collocTable{i}{j} = corrVBoth;  j = j + 1;
        collocTable{i}{j} = p_corrVBoth; j = j + 1;
        
        % Area based calculations:
        % Total threshold levels
        collocTable{i}{j} = sum(sum(imgRed > redT))/numel(imgRed); j = j + 1;
        collocTable{i}{j} = sum(sum(imgGreen > greenT))/numel(imgGreen);  j = j + 1;           
        
        % Jaccard index        
        collocTable{i}{j} = jaccardIdx; j = j + 1;
        collocTable{i}{j} = p_jaccardIdx; 

        currRedPostProcessDir = fullfile(postProcessedDir,inputFolders{redStain}); %#ok<PFBNS>
        if ~isfolder(currRedPostProcessDir), mkdir(currRedPostProcessDir); end
        currGreenPostProcessDir = fullfile(postProcessedDir,inputFolders{greenStain});
        if ~isfolder(currGreenPostProcessDir), mkdir(currGreenPostProcessDir); end
        
        imwrite(imresize(imbinarize(imgRed,redT_base),smallImgDownscaleFactor),fullfile(currRedPostProcessDir,[fnexexR '_ch' num2str(redStainChannel,'%02d') '_simpleThresh' '.png']));
        imwrite(imresize(imbinarize(imgGreen,greenT_base),smallImgDownscaleFactor),fullfile(currGreenPostProcessDir,[fnexexG '_ch' num2str(greenStainChannel,'%02d') '_simpleThresh' '.png']));
        %imwrite(imbinarize(imgRedTophat,redTtop_base),fullfile(currRedPostProcessDir,[fnexexR '_ch' num2str(redStainChannel,'%02d') '_topHatThresh' '.png']));
        %imwrite(imbinarize(imgGreenTophat,greenTtop_base),fullfile(currGreenPostProcessDir,[fnexexG '_ch' num2str(greenStainChannel,'%02d') '_topHatThresh' '.png']));

        fprintf('%d/%d\n',i,length(setIDs));
    end

    tableFile = fopen(fullfile(targetDir,sprintf('Collocalization_%s_vs_%s.csv',redStainName,greenStainName)),'w');
    for j=1:length(headerNames)-1, fprintf(tableFile,'%s,',headerNames{j}); end
    fprintf(tableFile,'%s\n',headerNames{end});
    for i=1:N
        for j=1:length(headerNames)-1, fprintf(tableFile,'%f,',collocTable{i}{j}); end
        fprintf(tableFile,'%f\n',collocTable{i}{end});
    end
    fclose(tableFile);
end
warning('on','MATLAB:table:RowsAddedExistingVars');
