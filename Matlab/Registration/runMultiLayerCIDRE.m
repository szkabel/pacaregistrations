%To Run CIDRE on multi-layered images

pathToCIDRE = 'D:\Abel\SZBK\Projects\Juho\PancreaticCancer\src\ThirdParty\CIDRE';
wrkPath = 'CIDRE-wrkDir';
cidreDest = 'CIDRE-dest';
nofLayers = 3; % e.g. RGB

addpath(genpath(pathToCIDRE));

pathToImgs = {...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\190219_Samples\TMA4B PC31 CK7'; ...
    'G:\Abel\SZBK\Projects\Juho\PancreaticCancer\190219_Samples\TMA4B_LDLR_PC26'; ...
    };
imgExt = 'tif';

layerWrkDir = cell(1,nofLayers);
for i=1:nofLayers
    layerWrkDir{i} = sprintf('CH-%02d',i); 
    if ~exist(fullfile(wrkPath,layerWrkDir{i}),'dir')
        mkdir(fullfile(wrkPath,layerWrkDir{i}));
    end
end

for k=1:length(pathToImgs)

    d = dir(fullfile(pathToImgs{k},['*.' imgExt ]));
    parfor ii=1:numel(d)
        fprintf('Preparing for CIDRE %d/%d...\n',ii,numel(d));        
        currSHGName  = d(ii).name;
        shgBF = imread(fullfile(pathToImgs{k},currSHGName)); %#ok<PFBNS>
        if(~all(shgBF(:)==255))
            for j=1:nofLayers                        
                imwrite(shgBF(:,:,j),fullfile(wrkPath,layerWrkDir{j},currSHGName)); %#ok<PFBNS>            
            end
        end
    end
end

%% Run CIDRE
fprintf('Running CIDRE...\n');
if ~exist(cidreDest,'dir'), mkdir(cidreDest); end
parfor j=1:nofLayers
    cidre(fullfile(wrkPath,layerWrkDir{j},['*.' imgExt]),'destination',fullfile(cidreDest,layerWrkDir{j}));
end

for i=1:length(pathToImgs)
    stem = strsplit(pathToImgs{i},filesep);
    stem = stem{end};
    if ~exist(fullfile(cidreDest,stem),'dir'), mkdir(fullfile(cidreDest,stem)); end    
    d = dir(fullfile(pathToImgs{i},['*.' imgExt ]));
    for j=1:numel(d)
        imgLays = cell(1,nofLayers);
        for k=1:nofLayers
            if exist(fullfile(cidreDest,layerWrkDir{k},d(j).name),'file')
                imgLays{k} = imread(fullfile(cidreDest,layerWrkDir{k},d(j).name));                            
            end
        end
        if ~isempty(imgLays{1})
            imwrite(cat(3,imgLays{:}),fullfile(cidreDest,stem,d(j).name));
        else
            copyfile(fullfile(pathToImgs{i},d(j).name),fullfile(cidreDest,stem,d(j).name));
        end
    end
end