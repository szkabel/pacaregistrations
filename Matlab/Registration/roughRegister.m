function [imgReg,ansCorr,roughTransBig] = roughRegister(imgFix,imgMove,showFig)
%Outputs a registered image based on the circular shape of the microarray.
%The last option is bool if we want to see the process


if nargin<3, showFig = false; end

[imgFix_prep,imgFix_sat] = preprocessForRoughAlign(imgFix);
[imgMove_prep,imgMove_sat] = preprocessForRoughAlign(imgMove);

% if showFig, figure; imshowpair(imgFix_prep,imgMove_prep); end
% 
% disp('Preprocessing is done');
% disp('Registration...');
% roughTrans = roughAlign(imgFix_prep,imgMove_prep);
% 
% disp('Warping...');
% imgMove_prep_RoughRegistered = imwarp(imgMove_prep,roughTrans,'OutputView',imref2d(size(imgFix_prep)));
% if showFig, figure; imshowpair(imgFix_prep,imgMove_prep_RoughRegistered); end
% 
% roughTransBig = roughTrans;
% roughTransBig.T(3,[1 2]) = (size(imgFix_sat)./size(imgFix_prep)).*roughTransBig.T(3,[1 2]);
% imgReg_sat = imwarp(imgMove_sat,roughTransBig,'OutputView',imref2d(size(imgFix_sat)));
% 
% %Measure the successful rate by correlation on the saturation image
% 
imgMove_sat_in_reg = imwarp(imgMove_sat,affine2d(eye(3)),'OutputView',imref2d(size(imgFix_sat)));
% oldCorr = corr2(imgMove_sat_in_reg,imgFix_sat);
% newCorr = corr2(imgReg_sat,imgFix_sat);
% improvement = (newCorr-oldCorr)/abs(oldCorr)*100;
% 
% fprintf('The old correlation was: %f\nThe new correlation is: %f\nThe improvement is: %4.2f%%\n',oldCorr,newCorr,improvement);

% if improvement<0
    imgReg = imgMove;
    roughTransBig = affine2d();
    roughTransBig.T = eye(3);
    ansCorr = corr2(imgFix_sat,imgMove_sat_in_reg);
% else
%     imgReg = imwarp(imgMove,roughTransBig,'OutputView',imref2d(size(imgFix)));
%     if showFig, figure; imshowpair(imgFix_sat,imgReg_sat); end
%     ansCorr = corr2(imgFix_sat,imgReg_sat);
% end


end

