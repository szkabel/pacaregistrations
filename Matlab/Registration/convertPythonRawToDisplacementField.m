function displacementField = convertPythonRawToDisplacementField(A,imgSize)
%The transformation script as mined out from the python lib

    imgSize = imgSize(1:2);
    [X,Y] = meshgrid(0:imgSize(1)-1,0:imgSize(2)-1);
    P = [X(:) Y(:)];
    A = reshape(A,2,3);
    B = [A; 0 0 1];
    B_rot = B(1:2,1:2);
    B_trans = B(1:2,3);
    transNeg = [-(imgSize(1)-1)/2 -(imgSize(2)-1)/2];
    transPos = [ (imgSize(1)-1)/2  (imgSize(2)-1)/2];
    PDisplaced = ((P+transNeg)*B_rot)+B_trans'+transPos;
    displacementByPoints = PDisplaced-P;
    displacementFieldX = reshape(displacementByPoints(:,1),imgSize(1),imgSize(2))';
    displacementFieldY = reshape(displacementByPoints(:,2),imgSize(1),imgSize(2))';

displacementField(:,:,1) = displacementFieldY;
displacementField(:,:,2) = displacementFieldX;