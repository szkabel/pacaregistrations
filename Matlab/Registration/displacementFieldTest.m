%Trial script to run exactly as in python to reproduce the same result
imgWidth = 5800;
[X,Y] = meshgrid(0:imgWidth-1);
P = [X(:) Y(:)];
%PHom = [P ones(size(P,1),1)];
BBB; % BBB should be the transformation matrix from python
BBB_rot = BBB(1:2,1:2);
BBB_trans = BBB(1:2,3);
transNeg = [-(imgWidth-1)/2 -(imgWidth-1)/2];
transPos = [(imgWidth-1)/2   (imgWidth-1)/2];
PDisplaced = ((P+transNeg)*BBB_rot)+BBB_trans'+transPos;
displacementByPoints = PDisplaced-P;
displacementFieldX = reshape(displacementByPoints(:,1),imgWidth,imgWidth)';
displacementFieldY = reshape(displacementByPoints(:,2),imgWidth,imgWidth)';
displacementField(:,:,1) = displacementFieldX;
displacementField(:,:,2) = displacementFieldY;
byDisplacement = imwarp(roughRegisteredImg,displacementField(:,:,[2 1]));
figure; imshow(byDisplacement)
figure; imshow(currOriginalImg)
figure; imshowpair(byDisplacement,imgNatasa)
figure; imshowpair(byDisplacement,trial4)