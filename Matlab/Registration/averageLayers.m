function [avgImg] = averageLayers(imgCellArray,downScaleFactor,weightArray)

if iscell(imgCellArray), imgCellArray = cat(3,imgCellArray{:}); end
rescSize = size(imresize(imgCellArray,1/downScaleFactor));
avgImg = zeros(rescSize(1),rescSize(2),class(imgCellArray));
nofLayers = size(imgCellArray,3);
for j=1:nofLayers
    if weightArray(1,j)~=0 && weightArray(2,j)~=0
        avgImg = avgImg + weightArray(1,j)*imadjust(imresize(imgCellArray(:,:,j),1/downScaleFactor),[],[],weightArray(2,j));
    end
end

%Transform back to original scale
avgImg = cast(avgImg,class(imgCellArray));
end

