% Requires the "smoothRegistered_MetaData.mat" to be loaded

load('G:\Abel\Research\Projects\Juho\PancreaticCancer\TMA\smoothRegistered_MetaData.mat');

selectedSpots = [37];
stainNames = cellfun(@strsplitN,inputFolders,repmat({'_'},length(inputFolders),1),repmat({2},length(inputFolders),1),'UniformOutput',false);

clear pixSizeTable;
origPixSize = 0.22;
for i=1:length(selectedSpots)
    tblLineX = cell(1,length(inputFolders)+1);
    tblLineY = tblLineX;
    tblLineX{1} = ['Spot ' num2str(selectedSpots(i)) ' x pixel size'];
    tblLineY{1} = ['Spot ' num2str(selectedSpots(i)) ' y pixel size'];
    for j=1:length(inputFolders)
        P = [origPixSize 0; 0 origPixSize];
        PP = P*inv(finalTransforms{selectedSpots(i)}.tforms{j}.T(1:2,1:2)); %#ok<MINV> I understand this better
        tblLineX{j+1} = norm(PP(1,:));
        tblLineY{j+1} = norm(PP(2,:));
    end
    pixSizeTable((i-1)*2+1,:) = cell2table(tblLineX,'VariableNames',['ID'; stainNames]);
    pixSizeTable((i-1)*2+2,:) = cell2table(tblLineY,'VariableNames',['ID'; stainNames]);
end

pixSizeTable(:,[1 3 4 8 10])