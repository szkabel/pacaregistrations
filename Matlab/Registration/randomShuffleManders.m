function [M1,M2,tM1,tM2,tbM1,tbM2,corrV,corrVBoth,jaccardIdx] = randomShuffleManders(imgRed,imgGreen,redT,greenT,nofIterations,blockSize)

%We are going to shuffle the green img. 
nR = size(imgGreen,1);
nC = size(imgGreen,2);
desiredSizeRow = ceil(nR/blockSize)*blockSize;
desiredSizeCol = ceil(nC/blockSize)*blockSize;
paddedGreen = padarray(imgGreen,[desiredSizeRow - nR, desiredSizeCol - nC ],'symmetric','post');

M1 = zeros(1,nofIterations);
M2 = zeros(1,nofIterations);
tM1 = zeros(1,nofIterations);
tM2 = zeros(1,nofIterations);
tbM1 = zeros(1,nofIterations);
tbM2 = zeros(1,nofIterations);
corrV = zeros(1,nofIterations);
corrVBoth = zeros(1,nofIterations);
jaccardIdx = zeros(1,nofIterations);

for i=1:nofIterations
        
    randGreen = randblock(paddedGreen,[blockSize blockSize]);
    
    randGreenOrigSize = randGreen(1:nR,1:nC);

    [M1(i),M2(i)] = MandersColoc(imgRed,randGreenOrigSize,0,0);
    [tM1(i),tM2(i)] = MandersColoc(imgRed,randGreenOrigSize,redT,greenT);
    [tbM1(i),tbM2(i)] = MandersColoc(imgRed,randGreenOrigSize,redT,greenT,true);
    corrV(i) = corr2(imgRed,randGreenOrigSize);
    unionImg = imgRed > redT | randGreenOrigSize > greenT;
    C = corrcoef(double(imgRed(unionImg)),double(randGreenOrigSize(unionImg)));
    corrVBoth(i) = C(1,2);
    intersectImg = imgRed > redT & randGreenOrigSize > greenT;
    jaccardIdx(i) = sum(intersectImg(:))/sum(unionImg(:));        

end

end

%{
function shuffledImg = randblock(img,blockSizes)
% img: the image to be shuffled in blocks
% blockSizes: vector of length 2 (as our imgs are 2D only) specifying the
% block sizes in each dimension

N = numel(img);
imgS = size(img);
blockS = [imgS(1)/blockSizes(1) imgS(2)/blockSizes(2)];
blockN = prod(blockS);
blockOrder = randperm(blockN);

linIdx = 1:N;

smallRowIdx = mod(linIdx-1,blockSizes(1))+1;
smallColIdx = mod(floor((linIdx-1)/imgS(1)),blockSizes(2))+1;
smallColsAhead = floor((linIdx-1)/blockSizes(1));
blockRow = mod(smallColsAhead,blockS(1))+1;
blockCol = floor(smallColsAhead/(blockSizes(2)*blockS(1)))+1;
blockLinIdx = (blockCol-1)*blockS(1)+blockRow;
blockLinIdx = blockOrder(blockLinIdx);

blockRowPerm = mod(blockLinIdx-1,blockS(1))+1;
blockColPerm = floor((blockLinIdx-1)/blockS(1))+1;

linIdxPerm = (blockColPerm-1)*prod(blockSizes)*blockS(1) + ... % full block cols ahead
                   (smallColIdx-1)*imgS(1) + ... %full columns ahead
                   (blockRowPerm-1)*blockSizes(1) + ... %block rows in our column
                   smallRowIdx;

shuffledImg = reshape(img(linIdxPerm),imgS);
               
end
%}
