% Export intensities based on color deconvolution and ROI masks into csv

baseDir = 'L:\ltdk_ikonen\Abel\Juho\PaCa\RevisionIHC\fullScreen\exports\Final';
colorDeconvDir = fullfile(baseDir,'deconvolved3');
maskDir = fullfile(baseDir,'masks');

tgtFile = 'L:\ltdk_ikonen\Abel\Juho\PaCa\RevisionIHC\fullScreen\exports\quants_Final3.csv';

chanOfInt = 1;
imgExt = 'png';

d = dir([colorDeconvDir '\*ch' num2str(chanOfInt) '.' imgExt]);

f = fopen(tgtFile,'w');
fprintf(f,'ImageID,antibody,roiClass,RegionID,sumIntensity,roiArea\n');
waba(0,numel(d),'st','Quantifying intensities')
for i = 1:numel(d)
    currImgName = d(i).name;
    [~,fileExEx,~] = fileparts(currImgName);
    
    imageID = strsplitN(fileExEx,'.',1:4);
    
    abImg = imread(fullfile(colorDeconvDir,currImgName)); %antibody image
    maskImg = imread(fullfile(maskDir,[imageID '.' imgExt]));
    
    values = abImg(maskImg>1);
    
    fprintf(f,'%s,%s,%s,%s,%f,%f\n',...
        strsplitN(strsplitN(fileExEx,'.',1),'_','1:end-1'),...
        strsplitN(strsplitN(fileExEx,'.',1),'_','end'),...
        strsplitN(fileExEx,'.',4),...
        strsplitN(fileExEx,'.',3),...
        sum(values),...
        length(values)...
    );
    waba(i,numel(d),'mid','Quantifying intensities')
end
fclose(f);
waba(i,numel(d),'end','Quantifying intensities')