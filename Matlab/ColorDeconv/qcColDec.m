function varargout = qcColDec(varargin)
% QCCOLDEC MATLAB code for qcColDec.fig
%      QCCOLDEC, by itself, creates a new QCCOLDEC or raises the existing
%      singleton*.
%
%      H = QCCOLDEC returns the handle to a new QCCOLDEC or the handle to
%      the existing singleton*.
%
%      QCCOLDEC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in QCCOLDEC.M with the given input arguments.
%
%      QCCOLDEC('Property','Value',...) creates a new QCCOLDEC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before qcColDec_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to qcColDec_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help qcColDec

% Last Modified by GUIDE v2.5 21-May-2022 16:12:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @qcColDec_OpeningFcn, ...
                   'gui_OutputFcn',  @qcColDec_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before qcColDec is made visible.
function qcColDec_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to qcColDec (see VARARGIN)

% Choose default command line output for qcColDec
handles.output = hObject;
if ~ischar(varargin{1})
    handles.UserData.currImg = varargin{1};
    handles.listbox1.String = {};
else    
    if exist(varargin{1},'dir')
        handles.UserData.imgDir = varargin{1};
        d = dir(handles.UserData.imgDir);
        fileNameList = {d.name};
        filter = regexp(fileNameList,'.*.(tif|tiff|jpg|png)');
        filter = ~cellfun(@isempty,filter);
        set(handles.listbox1,'String',fileNameList(filter));
        handles.listbox1.Value = 1;
        handles = listbox1_Callback(handles.listbox1, [], handles);
    else
        errordlg('Directory doesn''t exist!');
    end
end

% init colDec
handles = refreshDeconv(handles);
% save the settings so that changes can be detected
handles.UserData.ch1 = str2num(handles.edit1.String);
handles.UserData.ch2 = str2num(handles.edit2.String);

refreshImgView(handles);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes qcColDec wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = qcColDec_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when selected object is changed in uibuttongroup1.
function uibuttongroup1_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uibuttongroup1 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
refreshImgView(handles)


function refreshImgView(handles)
if (handles.radiobutton3.Value)
    if handles.minmaxStretch.Value
        imagesc(handles.UserData.currImg, 'Parent',handles.axes1)
    else
        imshow(handles.UserData.currImg, 'Parent',handles.axes1)
    end
else
    if (handles.radiobutton4.Value)
        ch = 1;    
    elseif (handles.radiobutton5.Value)
        ch = 2;
    end
    imgDeconvolved = handles.UserData.colDec;
    if handles.stretchOverList.Value     
        %minV = min(min(imgDeconvolved(:,:,ch)));
        %maxV = max(max(imgDeconvolved(:,:,ch)));
        minV = handles.UserData.setLims.minV(ch);
        maxV = handles.UserData.setLims.maxV(ch);
        im2show = (imgDeconvolved(:,:,ch) - minV) ./(maxV-minV);
    else
        im2show = imgDeconvolved(:,:,ch);
    end
    if handles.minmaxStretch.Value
        imagesc(im2show, 'Parent',handles.axes1);    
    else
        imshow(im2show, 'Parent',handles.axes1);    
    end
end



function handles = edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

if detectChange(handles) && handles.stretchOverList.Value && ~isempty(handles.listbox1.String)
    handles.UserData.setLims = calcStretchOverSet(handles);    
end

handles.UserData.ch1 = str2num(handles.edit1.String);
handles.UserData.ch2 = str2num(handles.edit2.String);

handles = refreshDeconv(handles);
refreshImgView(handles);
guidata(hObject, handles);


function bool = detectChange(handles)
if all(handles.UserData.ch1 == str2num(handles.edit1.String)) && ...
   all(handles.UserData.ch2 == str2num(handles.edit2.String))
    bool = false;
else
    bool = true;
end


% --- local help function for color deconvolution
function imgDeconvolved = calcDeconv(handles)

sampledPixels = {str2num(get(handles.edit1,'String'));str2num(get(handles.edit2,'String'))};
sampledPixels = cellfun(@(x)(repmat(x,2,1)),sampledPixels, 'UniformOutput',false);

currImg = handles.UserData.currImg;
maxImgVal = getMaxImgValFromClass(currImg);
for j=1:2
    cDM(j,:) = mean( -log(double(sampledPixels{j}+1)./maxImgVal) );
    cDM(j,:) = cDM(j,:)./norm(cDM(j,:)); %normalize to unit length
end
    
%This is specific here for 2 dyes.
cDM(3,:) = sqrt(max([0 0 0],ones(1,3)-sum((cDM(1:2,:)).^2)));

imgDeconvolved = SeparateStains(currImg, inv(cDM));


function handles = refreshDeconv(handles)
    imgDeconvolved = calcDeconv(handles);
    handles.UserData.colDec = imgDeconvolved;    


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox1.
function handles =  listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1

contents = cellstr(get(hObject,'String'));
handles.UserData.currImg = imread(fullfile(handles.UserData.imgDir,contents{get(hObject,'Value')}));
handles = refreshDeconv(handles);
refreshImgView(handles)
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in minmaxStretch.
function minmaxStretch_Callback(hObject, eventdata, handles)
% hObject    handle to minmaxStretch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of minmaxStretch
handles = refreshDeconv(handles);
refreshImgView(handles);
guidata(hObject, handles);


% --- Executes on button press in stretchOverList.
function handles = stretchOverList_Callback(hObject, eventdata, handles)
% hObject    handle to stretchOverList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of stretchOverList

if handles.stretchOverList.Value
    if ~isfield(handles.UserData,'setLims') && ~isempty(handles.listbox1.String)
        handles.UserData.setLims = calcStretchOverSet(handles);
    end
end
guidata(hObject,handles);

handles = refreshDeconv(handles);
refreshImgView(handles)
guidata(hObject, handles);

function setLims = calcStretchOverSet(handles)
    full = length(handles.listbox1.String);
    infoText = 'Identifying intensity range';
    waba(0,full,'st',infoText)
    % currSelection = handles.listbox1.Value;
    minV = Inf( 1,1,3);
    maxV = -minV;
    for i=1:full
        waba(i,full,'mid',infoText)
        handles.UserData.currImg = imread(fullfile(handles.UserData.imgDir,handles.listbox1.String{i}));
        imgDeconvolved = calcDeconv(handles);
        minV = min(minV,min(imgDeconvolved,[],[1 2]));
        maxV = max(maxV,max(imgDeconvolved,[],[1 2]));
    end
    waba(0,full,'end',infoText)
    setLims = struct('minV',minV,'maxV',maxV);
    
    % set back the current image
    % handles.UserData.currImg = imread(fullfile(handles.UserData.imgDir,handles.listbox1.String{currSelection}));


% --- Executes on button press in saveImgsButton.
function saveImgsButton_Callback(hObject, eventdata, handles, varargin)
% hObject    handle to saveImgsButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if ~isempty(handles.listbox1.String)
    if nargin>3
        tgtDir = varargin{1};
    else        
        tgtDir = uigetdir(handles.UserData.imgDir,'Choose a location to save');
    end
    
    full = length(handles.listbox1.String);
    infoText = 'Saving images';
    waba(0,full,'st',infoText)        
    for i=1:full
        waba(i,full,'mid',infoText)
        imgName = handles.listbox1.String{i};
        handles.UserData.currImg = imread(fullfile(handles.UserData.imgDir,imgName));
                
        for ch = 1:2
            deconvSetAdjusted = getCurrentDeconvSet(handles);
            [~,fileExEx,ext] = fileparts(imgName);
            imwrite(deconvSetAdjusted{ch},fullfile(tgtDir,[fileExEx '.ch' num2str(ch) ext]));        
        end
    end
    waba(0,full,'end',infoText)    
    f = fopen(fullfile(tgtDir,'channelSettings.txt'),'w');
    fprintf(f,'Channel 1: %s\n',handles.edit1.String);
    fprintf(f,'Channel 2: %s\n',handles.edit2.String);
    fclose(f);
else
    filter = {'*.tif';'*.tiff';'*.png';'*.jpg';'*.*'};
    [imgName,filePath] = uiputfile(filter,'Color deconvolved image set','colorDeconvolved');
    for ch = 1:2
        [~,fileExEx,ext] = fileparts(imgName);
        deconvSetAdjusted = getCurrentDeconvSet(handles);
        imwrite(deconvSetAdjusted{ch},fullfile(filePath,[fileExEx '.ch' num2str(ch) ext]));        
    end
    f = fopen(fullfile(filePath,['channelSettings_' fileExEx '.txt']),'w');
    fprintf(f,'Channel 1: %s\n',handles.edit1.String);
    fprintf(f,'Channel 2: %s\n',handles.edit2.String);
    fclose(f);
end

function deconvSetAdjusted = getCurrentDeconvSet(handles)
    imgDeconvolved = calcDeconv(handles);        
    deconvSetAdjusted = cell(2,1);
    for ch = 1:2
        im2show = imgDeconvolved(:,:,ch);
        if handles.stretchOverList.Value
            minV = handles.UserData.setLims.minV(ch);
            maxV = handles.UserData.setLims.maxV(ch);
            im2show = (im2show - minV) ./(maxV-minV);
        end            
        if  handles.minmaxStretch.Value
            minV = min(im2show,[],[1 2]);
            maxV = max(im2show,[],[1 2]);
            im2show = (im2show - minV) ./(maxV-minV);
        end        
        deconvSetAdjusted{ch} = im2show;
        
    end


% --- Executes on button press in stretchAndSaveBtn.
function stretchAndSaveBtn_Callback(hObject, eventdata, handles)
% hObject    handle to stretchAndSaveBtn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.stretchOverList,'Value',1)
set(handles.minmaxStretch,'Value',0)

tgtDir = uigetdir(handles.UserData.imgDir,'Choose a location to save');

handles = stretchOverList_Callback(handles.stretchOverList, [], handles);
saveImgsButton_Callback(handles.saveImgsButton,[],handles,tgtDir);

% Script to export the results, remember to set however the mask and output
% directory there correctly!
exportROIandColDecToCsv
