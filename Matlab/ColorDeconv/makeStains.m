function colorDeconvMatrices = makeStains(myStain,nofLayers)
%my Stain
%script to generate values to be used in colorDeconv: 
%import values as RGB to mystain -> reshape into colorDeconvMatrices
%rgb on columns, always 2 rows/stain: first stain then HE

stain = mat2cell(mat2cell(myStain,ones(1,nofLayers*2),3),repmat(2,1,nofLayers),1)';

colorDeconvMatrices = cell(nofLayers,1);

nofDyes = 2; % Note this is burnt in for 2
maxImgVal = 255; 
for i=1:nofLayers
    for j=1:nofDyes
        colorDeconvMatrices{i}(j,:) = -log(double(stain{i}{j}+1)./maxImgVal);
        colorDeconvMatrices{i}(j,:) = colorDeconvMatrices{i}(j,:)./norm(colorDeconvMatrices{i}(j,:)); %normalize to unit length
    end
    %This is specific here for 2 dyes.
    colorDeconvMatrices{i}(3,:) = sqrt(max([0 0 0],ones(1,3)-sum((colorDeconvMatrices{i}(1:2,:)).^2)));
end


