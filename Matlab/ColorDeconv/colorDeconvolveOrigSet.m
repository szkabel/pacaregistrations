%Sctipt to color deconvole RGB images based on previous color estimations
%and registrations.
%
%Some variables are needed to run this similar to those in
%runRoughRegistration

deconvolvedDir = fullfile(targetDir,'DeconvolvedOrig');

if ~isfolder(deconvolvedDir), mkdir(deconvolvedDir); end

limitsForLayers = cell(nofLayers,1);

deconvHists = cell(nofLayers,1);

for j=1:nofLayers    
    
   [minV,maxV,deconvHists{j}.HC,deconvHists{j}.edges] = calcDeconvLimits(inv(colorDeconvMatrices{j}),j,setIDs,finalFolder,formatStrings{j});
        
    parfor i=1:length(setIDs)
        currDir = fullfile(inputBase,inputFolders{j}); %#ok<PFBNS> small data
        currOutDir = fullfile(deconvolvedDir,inputFolders{j});
        fileName = sprintf(formatStrings{j},setIDs(i)); %#ok<PFBNS> small data
        [~,fnexex,ext] = fileparts(fileName);
        if ~isfolder(currOutDir), mkdir(currOutDir); end
        
        img = imread(fullfile(currDir,sprintf(formatStrings{j},setIDs(i)))); 
        imgDeconvolved = SeparateStains(img, inv(colorDeconvMatrices{j})); %#ok<PFBNS> small data
                
        tmpMin = repmat(minV,size(imgDeconvolved,1),size(imgDeconvolved,2));
        tmpMax = repmat(maxV,size(imgDeconvolved,1),size(imgDeconvolved,2));
        imgDeconvolved = uint8((imgDeconvolved-tmpMin)./(tmpMax-tmpMin)*255);
        imgDeconvolved(repmat(all(img == 0,3),1,1,3)) = 0;
       
        for k=1:3
            imwrite(imresize(imgDeconvolved(:,:,k),roughRescaleFactor), fullfile(currOutDir,[fnexex '_ch' num2str(k,'%02d') ext]));
        end
    end
    
    limitsForLayers{j}.minV = minV;
    limitsForLayers{j}.maxV = maxV;
end

save(fullfile(targetDir,'origDeconvLimits.mat'),'limitsForLayers','deconvHists');
