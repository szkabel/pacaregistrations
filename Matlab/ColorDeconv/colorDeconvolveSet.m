%Sctipt to color deconvole RGB images based on previous color estimations
%and registrations.
%
%Some variables are needed to run this similar to those in
%runRoughRegistration

deconvolvedDir = fullfile(targetDir,'Deconvolved');
deconvolvedDirSmall = fullfile(targetDir,'DeconvolvedSmall');
finalFolder = fullfile(targetDir,'FinalRegistrations');

if ~isfolder(deconvolvedDir), mkdir(deconvolvedDir); end
if ~isfolder(deconvolvedDirSmall), mkdir(deconvolvedDirSmall); end

if exist(fullfile(targetDir,'origDeconvLimits.mat'),'file')
    load(fullfile(targetDir,'origDeconvLimits.mat'),'limitsForLayers','deconvHists');
else
    deconvHists = cell(nofLayers,1);
end

if ~exist('limitsForLayers','var') || ( iscell(limitsForLayers) && all(cellfun(@isempty,limitsForLayers)))
    calcLimits = true;
    limitsForLayers = cell(nofLayers,1);
else    
    calcLimits = false;
end

for j=usedLayers'
    if calcLimits
        [minV,maxV,deconvHists{j}.HC,deconvHists{j}.edges] = calcDeconvLimits(inv(colorDeconvMatrices{j}),j,setIDs,finalFolder,formatStrings{j});
    else
        minV = limitsForLayers{j}.minV;
        maxV = limitsForLayers{j}.maxV;
    end
        
    fprintf('Running color deconvolution for stain %d\n',j);
    parfor i=1:length(setIDs)
        currDir = fullfile(finalFolder,num2str(setIDs(i),'%02d'));
        currOutDir = fullfile(deconvolvedDir,num2str(setIDs(i),'%02d'));
        fileName = sprintf(formatStrings{j},setIDs(i)); %#ok<PFBNS>
        [~,fnexex,ext] = fileparts(fileName);
        if ~exist(currOutDir,'dir'), mkdir(currOutDir); end
        
        img = imread(fullfile(currDir,sprintf(formatStrings{j},setIDs(i))));
        imgDeconvolved = SeparateStains(img, inv(colorDeconvMatrices{j})); %#ok<PFBNS>
        
        tmpMin = repmat(minV,size(imgDeconvolved,1),size(imgDeconvolved,2));
        tmpMax = repmat(maxV,size(imgDeconvolved,1),size(imgDeconvolved,2));
        imgDeconvolved = uint8((imgDeconvolved-tmpMin)./(tmpMax-tmpMin)*255);
        imgDeconvolved(repmat(all(img == 0,3),1,1,3)) = 0;
       
        for k=1:3
            imwrite(imgDeconvolved(:,:,k), fullfile(currOutDir,[fnexex '_ch' num2str(k,'%02d') ext]));
	    currOutSmallDir = fullfile(deconvolvedDirSmall,[inputFolders{j} '_ch' num2str(k,'%02d')]);
	    if ~exist(currOutSmallDir,'dir'), mkdir(currOutSmallDir); end
            imwrite(imresize(imgDeconvolved(:,:,k),smallImgDownscaleFactor), fullfile(currOutSmallDir,[fnexex '_ch' num2str(k,'%02d') ext]));
        end
    end
end

save(fullfile(targetDir,'deconvHists.mat'),'deconvHists')
