function [sampledPixels,roiPOS] = getColorSamplesFromImg(currImg,method)
%currImg: RGB image to sample from
%method: string defining if to try heuristically OR to select manually ('heuristic','manual')
%sampledPixels: cellarray as long as many dies we are aiming at

if nargin<2
    method = 'manual';
end

nofDies = 2;
sampledPixels = cell(1,nofDies);
roiPOS = cell(1,nofDies);

switch method
    case 'manual'
        %uiwait(msgbox('Please select region of interests containing almost pure stain!'))        
        
        f = figure();                
        f.WindowKeyPressFcn = @keyPressFunc;
        
        imshow(currImg);
        for i=1:nofDies
            roiPoly = drawpolygon();
            %input('Press enter when you are happy with your selection');
            uiwait(f);
            
            %Ask if there was good signal for dye x, after giving the
            %possibility to draw the roi for it
            answ = questdlg(['Was there proper dye #' num2str(i) ' on this image?'],'Image check','Yes','No','Yes');
            if strcmp(answ,'No')
                sampledPixels{i} = uint8(zeros(0,3));
                continue;
            end      
           
            pixMask = poly2mask(roiPoly.Position(:,1),roiPoly.Position(:,2),size(currImg,1),size(currImg,2));
            sampledPixels{i} = reshape(currImg(repmat(pixMask,1,1,3)),[],3);
            roiPOS{i} = roiPoly.Position;
            
            if isempty(sampledPixels{i}), sampledPixels{i} = uint8(zeros(0,3)); end
        end        
        close(f);                
    case 'heuristic'
        currImg_OD = transformImgToOpticalDensity(currImg);
        currImg_OD_ratio = currImg_OD(:,:,[1 1 2])./currImg_OD(:,:,[2 3 3]); %pairwise ratio (this is what should be constant for a given dye but it just simply isn't)        
        %However we experienced that with some rule of thumbs the different
        %colors still should be somehow extractable. This is completely
        %heuristical or user defined by selecting rois.
                
        brownPixelsLogical = 0.75*(currImg_OD_ratio(:,:,1) + currImg_OD_ratio(:,:,2)) < currImg_OD_ratio(:,:,3); %look blue on rat img
        bluePixelsLogical = 0.75*(currImg_OD_ratio(:,:,1) + currImg_OD_ratio(:,:,3)) < currImg_OD_ratio(:,:,2); %look green on rat img
        
        %Only for first checks
        %figure; imagesc(brownPixelsLogical);
        %figure; imagesc(currImg);
        %figure; imagesc(bluePixelsLogical);
        %input('Press enter to continue...','s');
        
        sampledPixels{1} = reshape(currImg(repmat(bluePixelsLogical,1,1,3)),[],3);
        sampledPixels{2} = reshape(currImg(repmat(brownPixelsLogical,1,1,3)),[],3);
        
        [blueX,blueY] = ind2sub(size(bluePixelsLogical),find(bluePixelsLogical));
        [brownX,brownY] = ind2sub(size(brownPixelsLogical),find(brownPixelsLogical));
        roiPOS{1} = [blueX blueY];
        roiPOS{2} = [brownX brownY];
end

end

function keyPressFunc(hFig,eventdata,~)
    switch eventdata.Character
        case 'z'
            pan off;
            zoom on;        
        case 's'
            zoom off;
            pan off;
    end    
    if strcmp(eventdata.Key,'space')
        zoom off;
        pan on;
    end
    % This should work in both HG1 and HG2:
    hManager = uigetmodemanager(hFig);
    try
        set(hManager.WindowListenerHandles, 'Enable', 'off');  % HG1
    catch
        [hManager.WindowListenerHandles.Enabled] = deal(false);  % HG2
    end
    set(hFig, 'WindowKeyPressFcn', []);
    set(hFig, 'KeyPressFcn', @keyPressFunc);
    if strcmp(eventdata.Key,'return')
        uiresume(hFig);
    end
end

