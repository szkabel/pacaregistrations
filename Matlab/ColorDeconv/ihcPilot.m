% piloting IHC quantification

currImg = imread('L:\ltdk_ikonen\Abel\Juho\PaCa\RevisionIHC\testStainingImgs\2022_04_02_12_00_58--ap2m1 1_200 27355-a-ap 12-514-88 insets\whiteBalanced\ca1 inset.tif');

[sampledPixels,roiPOS] = getColorSamplesFromImg(currImg);

sampledPixels = {[111 93 65];[50 64 120]};
sampledPixels = cellfun(@(x)(repmat(x,2,1)),sampledPixels, 'UniformOutput',false);

maxImgVal = getMaxImgValFromClass(currImg);
for j=1:2
    cDM(j,:) = mean( -log(double(sampledPixels{j}+1)./maxImgVal) );
    cDM(j,:) = cDM(j,:)./norm(cDM(j,:)); %normalize to unit length
end
    
%This is specific here for 2 dyes.
cDM(3,:) = sqrt(max([0 0 0],ones(1,3)-sum((cDM(1:2,:)).^2)));

imgDeconvolved = SeparateStains(currImg, inv(cDM));

minV = min(min(imgDeconvolved(:,:,1)));
maxV = max(max(imgDeconvolved(:,:,1)));
cancerAP2M = (imgDeconvolved(:,:,1) - minV) ./(maxV-minV);
figure; imagesc(cancerAP2M)


currImg = imread('L:\ltdk_ikonen\Abel\Juho\PaCa\RevisionIHC\testStainingImgs\2022_04_02_12_00_58--ap2m1 1_200 27355-a-ap 12-514-88 insets\he1 inset.tif');
imgDeconvolved = SeparateStains(currImg, inv(cDM));
healthyAP2M = (imgDeconvolved(:,:,2) - minV) ./(maxV-minV);
