function [minV,maxV,HC,histEdges] = calcDeconvLimits(colDeconvMatrix,j,setIDs,finalFolder,currFS,varargin)
%Function to calculate the minimum and maximum valuesfor a given channel.
%Optional argument is the quantiles to define how much percentage to cut
%off from the values. It must be always of length 2 (beacuse there is only
%a single min and max cutoff), and in increasing order.
%Both values are cutting of *AT LEAST* the specified percentage.

p = inputParser();
p.addParameter('quantiles',[0.5,0.9999]);
p.parse(varargin{:});
q = p.Results.quantiles;

histResolution = 65536;

[chanMin,chanMax] = calcExtremePointDeconvs(colDeconvMatrix);
histEdges = cell(1,3);
for k=1:3, histEdges{k} = linspace(chanMin(k),chanMax(k),histResolution); end
histCounts = zeros(length(setIDs),histResolution-1,3);

fprintf('Calculating color deconv-limits for stain %d\n',j);
minV = zeros(1,1,3);
maxV = zeros(1,1,3);

%parfor i=1:length(setIDs)
parfor i=1:length(setIDs)
    currDir = fullfile(finalFolder,num2str(setIDs(i),'%02d'));

    img = imread(fullfile(currDir,sprintf(currFS,setIDs(i))));
    imgDeconvolved = SeparateStains(img,colDeconvMatrix);

    for k=1:3                
        currDeconvChan = imgDeconvolved(:,:,k);
        histCounts(i,:,k) = histCounts(i,:,k) + histcounts(currDeconvChan(~all(img == 0,3)),histEdges{k});             
    end
end    

HC = sum(histCounts,1);
HCcdf = cumsum(HC,2)./sum(HC,2);
%This should be always 2, as there is only a single min and max cutoff
for k=1:3
    percentMinIdx = find(HCcdf>q(1),1,'first');
    percentMaxIdx = find(HCcdf>q(2),1,'first');
    minV(k) = histEdges{k}(percentMinIdx+1);
    maxV(k) = histEdges{k}(percentMaxIdx);
end
            
end

function [chanMin,chanMax] = calcExtremePointDeconvs(deconvMatrix)
%INPUT: deconv matrix for a given channel
%OUTPUT: 1 by 3 vectors the the possible min and max values if the input
%image can range from 2 to 257 (and the transformation is done by -log(img+2)*deconvMatrix)

extremePoints = zeros(8,3);

for iii=0:7
    extremePoints(iii+1,:) = cellfun(@str2double,num2cell(dec2bin(iii,3)))*255+2;
end

extremeDeconvs = -log(extremePoints)*deconvMatrix;
chanMin = min(extremeDeconvs,[],1);
chanMax = max(extremeDeconvs,[],1);


end

