%v4
load('20200410_121159_ColorDeconvParameters.mat','sampledPixels','colorDeconvMatrices','roiPOS','selectedIDsForLayer');
%v3
load('20200409_123702_ColorDeconvParameters.mat','sampledPixels','colorDeconvMatrices','roiPOS','selectedIDsForLayer');
nofLayers = size(colorDeconvMatrices,2);
stain = cell(1,nofLayers);

samplingNumber = 5; %How many images to consider when calculating the color deconvolution values
sampledPixels = cell(samplingNumber,nofLayers);
roiPOS = cell(samplingNumber,nofLayers);
mergedPixelsPerDye = cell(1,nofLayers);
colorDeconvMatrices = cell(1,nofLayers);
selectedIDsForLayer = cell(1,nofLayers);

for i=1:nofLayers
    singleDye = vertcat(sampledPixels{:,i});
    nofDyes = size(singleDye,2);
    mergedPixelsPerDye{i} = cell(1,nofDyes);
    for j=1:nofDyes
        mergedPixelsPerDye{i}{j} = cell2mat(singleDye(:,j));
    end
    
    %It is supposed that all img have the same format
    %These are all 8-bit images, perhaps still safer to have 255 as max?
    maxImgVal = 255; 
    
    for j=1:nofDyes
        colorDeconvMatrices{i}(j,:) = mean( -log(double(mergedPixelsPerDye{i}{j}+1)./maxImgVal) );
        colorDeconvMatrices{i}(j,:) = colorDeconvMatrices{i}(j,:)./norm(colorDeconvMatrices{i}(j,:)); %normalize to unit length
        stain{i}(j,:) = mean(mergedPixelsPerDye{i}{j});
    end
    
    %This is specific here for 2 dyes.
    colorDeconvMatrices{i}(3,:) = sqrt(max([0 0 0],ones(1,3)-sum((colorDeconvMatrices{i}(1:2,:)).^2)));
   
end

stain = cell2mat(transpose(stain));
stain
