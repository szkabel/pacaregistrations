function plotInOtherSpace(img,space)

switch space
    case 'lab'
        imgOther = rgb2lab(img);
    case 'hsv'
        imgOther = rgb2hsv(img);
    case 'ntsc'
        imgOther = rgb2ntsc(img);
end

figure;
subplot(1,3,1);
imagesc(imgOther(:,:,1));
subplot(1,3,2);
imagesc(imgOther(:,:,2));
subplot(1,3,3);
imagesc(imgOther(:,:,3));


end