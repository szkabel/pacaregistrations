function colorDeconvMatrices = estimateColorDeconvMatrix(nofLayers,setIDs,inputBase,inputFolders,targetDir,formatStrings)
%A function to estimate the color deconvolution matrix from our images.
%Input variables are mostly set by runRoughRegistration script

samplingNumber = 5; %How many images to consider when calculating the color deconvolution values
sampledPixels = cell(samplingNumber,nofLayers);
roiPOS = cell(samplingNumber,nofLayers);
mergedPixelsPerDye = cell(1,nofLayers);
colorDeconvMatrices = cell(1,nofLayers);
selectedIDsForLayer = cell(1,nofLayers);

paramTgtDir = fullfile(targetDir,'DeconvParameters_save');
if ~isfolder(paramTgtDir), mkdir(paramTgtDir); end

%load('20191028_103127_ColorDeconvParameters.mat','sampledPixels','colorDeconvMatrices','roiPOS','selectedIDsForLayer');

for i=1:nofLayers
    selectedIDsForLayer{i} = selectIDsForLayer('manual',i,inputBase,inputFolders,setIDs);           
    samplingNumber = length(selectedIDsForLayer{i});
        
    for j = 1:samplingNumber
        currImg = imread(fullfile(inputBase,inputFolders{i},sprintf(formatStrings{i},selectedIDsForLayer{i}(j))));
        [sampledPixels{j,i},roiPOS{j,i}] = getColorSamplesFromImg(currImg,'manual');
        
        %Extra save
        save(fullfile(paramTgtDir, [ timeString(6) '_ColorDeconvParameters.mat']),'sampledPixels','colorDeconvMatrices','roiPOS','selectedIDsForLayer');
    end
    singleDye = vertcat(sampledPixels{:,i});
    nofDyes = size(singleDye,2);
    mergedPixelsPerDye{i} = cell(1,nofDyes);
    for j=1:nofDyes
        mergedPixelsPerDye{i}{j} = cell2mat(singleDye(:,j));
    end
    
    %It is supposed that all img have the same format
    maxImgVal = getMaxImgValFromClass(currImg);
        
    for j=1:nofDyes
        colorDeconvMatrices{i}(j,:) = mean( -log(double(mergedPixelsPerDye{i}{j}+1)./maxImgVal) );
        colorDeconvMatrices{i}(j,:) = colorDeconvMatrices{i}(j,:)./norm(colorDeconvMatrices{i}(j,:)); %normalize to unit length
    end
    
    %This is specific here for 2 dyes.
    colorDeconvMatrices{i}(3,:) = sqrt(max([0 0 0],ones(1,3)-sum((colorDeconvMatrices{i}(1:2,:)).^2)));
    
    %Extra save
    save(fullfile(paramTgtDir, [ timeString(6) '_ColorDeconvParameters.mat']),'sampledPixels','colorDeconvMatrices','roiPOS','selectedIDsForLayer');
end

save(fullfile(paramTgtDir, [ strrep(strrep(datestr(datetime),' ','_'),':','-') '_ColorDeconvParameters.mat']),'sampledPixels','colorDeconvMatrices','roiPOS');

end

function selectedIDsForThisLayer = selectIDsForLayer(method,layerIdx,inputBase,inputFolders,setIDs)
    switch method
        case 'random'
            selectedIDsForThisLayer = setIDs(randperm(length(setIDs)));
        case 'manual'
            currDir = fullfile(inputBase,inputFolders{layerIdx});
            winopen(currDir);
            pause(1)
            imgList = dir(currDir);
            L = {imgList.name};
            L = L(~[imgList.isdir]);
            [selectedIDsForThisLayer,~] = listdlg('ListString',L,'SelectionMode','multiple','PromptString','Select images:','ListSize',[250,500]);
    end
end

