% Estimates the image feature (as required by the WhiteBalance sRGB module)
% from a set of images that can be located in multiple folders.
%   INPUTS:
%       model   The WBmodel that'll be used for the correction. Needed so
%               that the same features can be extracted from the new image
%               sets as it was from the training set during training.
%       inputDirs Cellarray with 3 columns. Each row encodes a directory to
%               read images from. First column is the directory, second is
%               a possible regex for the file (* for all images) and third
%               is the extension to be used in that folder
function featureMM = estimateFeatureFromSet(model, inputDirs) 
    if nargin==1
        % Here one can make this more like a script
        baseDir = 'L:\ltdk_ikonen\Abel\Juho\PaCa\RevisionIHC\testStainingImgs\';
        inputDirs = {...
           [baseDir filesep '2022_04_02_12_00_58--ap2m1 1_200 27355-a-ap 12-514-88 insets'],'*','tif';...
           [baseDir filesep '2022_04_02_13_05_13--nceh1_1-200_HPA-026888_12-514-88_insets'],'*','tif';...
           };
    end
    featureC = cell(size(inputDirs,1),1);
    labelC = cell(size(inputDirs,1),1);
    for i=1:size(inputDirs,1)
        d = dir([inputDirs{i,1} filesep inputDirs{i,2} '.' inputDirs{i,3}]);
        featureC{i} = cell(numel(d),1);
        labelC{i} = cell(numel(d),1);
        for j=1:numel(d)
            img = imread(fullfile(d(j).folder,d(j).name));
            hist = model.RGB_UVhist(im2double(img));
            featureC{i}{j} = model.encode(hist);
            labelC{i}{j} = [num2str(i) '-' d(j).name];
        end        
    end
    
    featureM = cell2mat(vertcat(featureC{:}));
    featureMM = mean(featureM,1);
    labelC = vertcat(labelC{:});
    
    % Some local EDA here:
    % Plot the features with labels
% {
    figure;
    plot(double(featureM(:,1)),double(featureM(:,2)),'g.'); hold on; 
    plot(featureMM(1),featureMM(2),'r*');
    text(double(featureM(:,1)),double(featureM(:,2)),labelC); hold off;
    
    figure; 
    plot(model.features(:,1),model.features(:,2),'.'); hold on;
    plot(featureMM(1),featureMM(2),'r*','MarkerSize',12);
    plot(double(featureM(:,1)),double(featureM(:,2)),'g.','MarkerSize',12); hold off;
% }
end