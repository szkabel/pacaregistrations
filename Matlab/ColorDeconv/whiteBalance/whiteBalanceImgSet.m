% White-Balance image set
% Using the WB_sRGB package from 2019_CVPR_Afifi paper

% Load in the WB model (found in the repo), use the upgraded version
load('models/WB_model+.mat','model');

baseDir = 'L:\ltdk_ikonen\Abel\Juho\PaCa\RevisionIHC\testStainingImgs\';
inputDirs = {...
   [baseDir filesep '2022_04_02_12_00_58--ap2m1 1_200 27355-a-ap 12-514-88 insets'],'*','tif';...
   [baseDir filesep '2022_04_02_13_05_13--nceh1_1-200_HPA-026888_12-514-88_insets'],'*','tif';...
 };

% Define here the averaged features for the image set
feature = estimateFeatureFromSet(model, inputDirs);

% Then do the correction img by img
srcDirs = {...
    [baseDir filesep '2022_04_02_12_00_58--ap2m1 1_200 27355-a-ap 12-514-88 insets'];...
    [baseDir filesep '2022_04_02_13_05_13--nceh1_1-200_HPA-026888_12-514-88_insets']...
    };
for iii = 1:length(srcDirs) 
    srcDir = srcDirs{iii};
    imgRegEx = '*.tif';
    tgtDir = [srcDir filesep 'whiteBalanced/all'];

    if ~exist(tgtDir,'dir'), mkdir(tgtDir); end

    d = dir(fullfile(srcDir,imgRegEx));
    pfm = 0;
    for i=1:numel(d)
        img = imread(fullfile(d(i).folder,d(i).name));
        % returning fm as well. If it worked out correctly it should be a
        % constant
        [img_corr,fm] = model.correctImage(img,feature);
        if size(pfm,1)~=1 && all(all(fm == pfm))
            fprintf('In iteration %d the mapping is identical to the previous one.\n',i);
            % great!
        end
       pfm = fm;
        imwrite(img_corr, fullfile(tgtDir,d(i).name));
    end
end
