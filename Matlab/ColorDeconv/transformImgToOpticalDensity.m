function [imgOD] = transformImgToOpticalDensity(imgRGB)
%Transforms the image to optical density (from the detected intensity space)
%The input should be an RGB image.

% Optionally this should be extended with some white bg detection

imgOD = zeros(size(imgRGB));

switch class(imgRGB)
    case 'uint8'
        maxImgVal = 255;
    case 'uint16'
        maxImgVal = 65535;
    otherwise        
        maxImgVal = 1;
end
for i=1:3
    imgOD(:,:,i) = -log(double(imgRGB(:,:,i))./maxImgVal);
end

end

