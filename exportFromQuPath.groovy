/*Dialogs.showPlainMessage('My dialog',
    """This little script will say hello.
    
    Hello.""");
*/

// So is this JAVA then? Probably.
// No, sg called groovy

print "Hello world!\n"
// Ok, this will lead to quite some googling (Well, also without that, I forgot Java mostly :D //

import static qupath.lib.gui.scripting.QPEx.*
import qupath.lib.objects.PathObjects
import qupath.lib.roi.ROIs
import qupath.lib.regions.ImagePlane
import ij.*
import javax.imageio.ImageIO
import java.awt.Color

def project = getProject()

def imgPath = buildFilePath("L:/ltdk_ikonen/Abel/Juho/PaCa/RevisionIHC/fullScreen/exports/FirstFullCorrected");
new File(imgPath).mkdir()
def maskPath = buildFilePath(imgPath,"masks");
new File(maskPath).mkdir()


for (entry in project.getImageList()) {
    def imageData = entry.readImageData()
    def hierarchy = imageData.getHierarchy()
    def annotations = hierarchy.getAnnotationObjects()
    int i = 1
    def server = imageData.getServer()
    print "Processing image " + entry.getImageName() 
    for (annot in annotations) {        
        def currROI = annot.getROI();
        def request = RegionRequest.createInstance(server.getPath(), 1.0, currROI)
        def img = server.readBufferedImage(request)
        def imgName = entry.getImageName() + "." + i + "." + annot.getPathClass()
        ImageIO.write(img, "png", new File(imgPath + "/" + imgName + ".png"));
        
        // Make here then also the binary mask
        def shape = currROI.getShape()
        def imgMask = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_BYTE_GRAY)
        def g2d = imgMask.createGraphics()
        g2d.scale(1.0/request.getDownsample(), 1.0/request.getDownsample())
        g2d.translate(-request.getX(), -request.getY())
        g2d.setColor(Color.WHITE)
        g2d.fill(shape)
        g2d.dispose()
        ImageIO.write(imgMask, "png", new File(maskPath + "/" + imgName +".png"));

        print "ROI " + i + "/" + annotations.size() + " DONE."
        i++;
    }
}

print "ALL DONE!"