import time
import sys
import os
import random
import numpy as np
import multiprocessing as mp

# Info for the users and startup
# This script needs to be callable by command line parameters so that finally the whole process can be called from
# bash or preferably from Matlab
#
# Command line arguments for this project:
# 1) The number of layers (stainings) [N]
# 2) The input base folder: this where the images are located, this where the reference images are coming from
# 3) The target folder. Before running this script there should be another folder named RoughRegistered in which
#    there should be other folders with x_y naming conventions where both x and y range from 1 to the number of stains
#    used.Not all combinations are required, only all possible pairs with increasing order.
# 4->4+N) The folder names relative to input base folder (2)
# 4+N->4+2N) The regular expression strings for the files within that

def runRegOnImgList(curr_move_img,target_dir,inputRegEx,fiximg_dir_int,fiximg_dir_path,rr_img_path,smooth_dir,pairwise_dirs,i):
	import register_example2
	
	tmp_file = open(os.path.join(smooth_dir, pairwise_dirs[i], "StartIndicator_" + curr_move_img + ".txt"), 'w')
	tmp_file.write("Image registration has started.\n")
	
	#curr_move_img = rough_registered_imgs[j]
	print("Starting registration...")	
	(fileExex, _) = os.path.splitext(curr_move_img)
	lastFIdx = fileExex.rfind("_F")	
	fieldID = int(fileExex[lastFIdx+2:])

	orig_fixed_img = inputRegEx[fiximg_dir_int].format(fieldID)

	orig_fixed_full = os.path.join(fiximg_dir_path, orig_fixed_img)
	move_img_full = os.path.join(rr_img_path, curr_move_img)
	
	target_dir = os.path.join(target_dir,'tmp',fileExex)

	tmp_file.write("Preprocessing is done.\n")

	tform = register_example2.main(orig_fixed_full, move_img_full, target_dir)
	
	tmp_file.write("Main algorithm has finished.\n")
	print("Transform from image: {} \nto image {} is:\n {}".format(orig_fixed_full, move_img_full, tform.get_params()))

	smooth_transform_file = open(os.path.join(smooth_dir, pairwise_dirs[i], "SmoothTransform_" + fileExex[11:] + ".txt"), 'w')
	Tform = tform.get_params()
	for k in range(len(Tform)):
		smooth_transform_file.write(str(Tform[k]))
		smooth_transform_file.write(" ")
	smooth_transform_file.close()
	
	tmp_file.write("Trafo has been written to disk.\n")
	
	tmp_file.close()
	

def myStupidSimpleTest(curr_move_img,targetDir,inputRegEx,i):
	print("My move img: {}".format(curr_move_img))
	print("My target dir: {}".format(targetDir))
	print("My index is: {}".format(i))
	print("My input regexes are:")
	for ii in range(1,len(inputRegEx)):
		print(inputRegEx[ii])


if __name__ == '__main__':

	start_time = time.time()

	#print("The following file was called: " + sys.argv[0] + "\nThe parameters used:")
	#for i in range(1, len(sys.argv)):
	#	print(sys.argv[i])
	
	input_base_folder = sys.argv[2]
	target_dir = sys.argv[4]
	nofThreads = int(sys.argv[5])

	nofLayers = int(sys.argv[1])
	inputFolders = list()
	for i in range(1,nofLayers):
		inputFolders.append(sys.argv[5+i])

	inputRegEx = list()
	for i in range(1,nofLayers):
		if sys.argv[3] != "ilastik":
			inputChannelID = int(sys.argv[3])
			(fileExex,ext) = os.path.splitext(sys.argv[5+nofLayers+i])
			inputRegEx.append(fileExex + "_ch" + "{:02d}".format(inputChannelID) + ext)
		else:
			inputRegEx.append(sys.argv[5+nofLayers+i])

	# ***********************
	# Preprocessing

	# Random seeds for reproducibility
	random.seed(20010911)
	np.random.seed(19650910)

	smooth_dir = os.path.join(target_dir, "SmoothRegistrations")
	rough_registered_dir = os.path.join(target_dir, "RoughRegistered")

	if not os.path.exists(rough_registered_dir):
		raise Exception("Rough registered folder doesn't exist")

	if not os.path.exists(smooth_dir):
		os.makedirs(smooth_dir)

	pairwise_dirs = os.listdir(rough_registered_dir)
	

	for i in range(len(pairwise_dirs)):
	#for i in np.array([9,10,11,12,13,14,15,16]):
		separated_dirs = pairwise_dirs[i].split("_")
		print("Fix: {} moving: {}".format(separated_dirs[0], separated_dirs[1]))

		fiximg_dir_int = int(separated_dirs[0])-1
		fiximg_dir_path = os.path.join(input_base_folder, inputFolders[fiximg_dir_int])
		rr_img_path = os.path.join(rough_registered_dir, pairwise_dirs[i])

		rough_registered_imgs = os.listdir(rr_img_path)

		if not os.path.exists(os.path.join(smooth_dir, pairwise_dirs[i])):
			os.makedirs(os.path.join(smooth_dir, pairwise_dirs[i]))

		#for j in range(1, len(rough_registered_imgs)):        
		#for currRegImg in rough_registered_imgs:
		#	runRegOnImgList(currRegImg, target_dir,inputRegEx,fiximg_dir_int,fiximg_dir_path,rr_img_path,smooth_dir,pairwise_dirs,i)

		pool = mp.Pool(nofThreads)
		#pool.starmap_async(myStupidSimpleTest,[(currRegImg, target_dir,inputRegEx,i) for currRegImg in rough_registered_imgs])
		pool.starmap_async(runRegOnImgList, [(currRegImg, target_dir,inputRegEx,fiximg_dir_int,fiximg_dir_path,rr_img_path,smooth_dir,pairwise_dirs,i) for currRegImg in rough_registered_imgs])
		#for currRegImg in rough_registered_imgs:
		#	runRegOnImgList(currRegImg,target_dir, inputRegEx, fiximg_dir_int, fiximg_dir_path, rr_img_path, smooth_dir, pairwise_dirs,i)

		pool.close()
		pool.join()

	# print("Stop here to explore python")

	end_time = time.time()

	print("Elapsed time: " + str((end_time-start_time)))
