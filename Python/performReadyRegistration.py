import numpy as np

import scipy

from transforms import AffineTransform
from transforms import CompositeTransform
import transforms
import os
import PIL

PIL.Image.MAX_IMAGE_PIXELS = 900000000


baseTrafo = AffineTransform(2)
#downScaleTrafo = AffineTransform(2)
#upScaleTrafo = AffineTransform(2)
scaleFactor = 4

baseTrafo.set_params(np.array([0.90515522, -0.01001225, 0.01528469, 0.90532472, -12.50102547, 23.10934897]))
# baseTrafo.set_params(np.array([1, 0, 0, 1, 0, 0]))
# downScaleTrafo.set_params(np.array([1/scaleFactor, 0, 0, 0, 1/scaleFactor, 0]))
# upScaleTrafo.set_params(np.array([scaleFactor, 0, 0, 0, scaleFactor, 0]))

# Filepaths to run

flowFullPath = "D:/Abel/SZBK/Projects/Juho/tmp/transfer_30908_files_a44e38f0/C3-A03_HERO_small.png"
# Ref img path is only needed for it's shape
refFullPath = "D:/Abel/SZBK/Projects/Juho/tmp/transfer_30908_files_a44e38f0/A03_SHG_EPI_small.png"

ref_im = scipy.misc.imread(refFullPath, 'L')
flo_im = scipy.misc.imread(flowFullPath, 'L')

# trafoToBeRun = CompositeTransform(2, [upScaleTrafo, baseTrafo, downScaleTrafo])
trafoToBeRun = CompositeTransform(2, [baseTrafo])

centeredTrafo = transforms.make_image_centered_transform(trafoToBeRun, ref_im, flo_im)

refImWarped = np.zeros(ref_im.shape)

spacing = np.array([1.0, 1.0])
centeredTrafo.warp(In=flo_im, Out=refImWarped, mode='spline', in_spacing=spacing, out_spacing=spacing)

(floPath, floFileName) = os.path.split(flowFullPath)
(floFileExEx, ext) = os.path.splitext(floFileName)
registerPath = os.path.join(floPath, 'registered')

if not os.path.exists(registerPath):
    os.makedirs(registerPath)

scipy.misc.imsave(os.path.join(registerPath, floFileExEx + "_test" + ext), refImWarped)

