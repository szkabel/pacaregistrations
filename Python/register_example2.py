
#
# Py-Alpha-AMD Registration Framework
# Author: Johan Ofverstedt
# Reference: Fast and Robust Symmetric Image Registration Based on Distances Combining Intensity and Spatial Information
#
# Copyright 2019 Johan Ofverstedt
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# Modified by Abel Szkalisity in 2019.
#

#
# Example script for affine registration
#

# Import Numpy/Scipy
import numpy as np
import scipy as sp
from skimage import io
import os
import sys

sys.path.insert(1, os.path.join(sys.path[0], '../../ThirdParty/py_alpha_amd_release/'));

# Import transforms
from transforms import CompositeTransform
from transforms import AffineTransform
from transforms import Rigid2DTransform
from transforms import Rotate2DTransform
from transforms import TranslationTransform
from transforms import ScalingTransform
import transforms

#import matplotlib
#import imshowpair
#import utils

# Import optimizers
from optimizers import GradientDescentOptimizer

# Import generators and filters
import generators
import filters

# Import registration framework
from register import Register

# Import misc
import math
import time
import os

# Registration Parameters
alpha_levels = 7
# Pixel-size
spacing = np.array([1.0, 1.0])
# Run the symmetric version of the registration framework
symmetric_measure = True
# Use the squared Euclidean distance
squared_measure = False

# The number of iterations
param_iterations = 3000
# The fraction of the points to sample randomly (0.0-1.0)
param_sampling_fraction = 0.1
# Number of iterations between each printed output (with current distance/gradient/parameters)
param_report_freq = 500

def main(ref_im_path,flo_im_path,output_path,downScale = 1):
	#Random seed should be set outside

	(rdrive, rpath) = os.path.splitdrive(ref_im_path)
	(rpath, rfilename) = os.path.split(rpath)

	(fdrive, fpath) = os.path.splitdrive(flo_im_path)
	(fpath, ffilename) = os.path.split(fpath)

	print('Drive is %s Path is %s and file is %s' % (rdrive, rpath, rfilename))
	(curr_img_string, ext) = os.path.splitext(rfilename)
	split_curr_img = curr_img_string.split("_")
	reg_type = split_curr_img[1]
	reg_num = split_curr_img[3]
	print(reg_type)
	print(reg_num)

	# return True;

	print("Original images are being read")
	ref_im = io.imread(ref_im_path, as_gray=True)
	flo_im = io.imread(flo_im_path, as_gray=True)

	# Save copies of original images
	ref_im_orig = ref_im.copy()
	flo_im_orig = flo_im.copy()

	ref_im = filters.normalize(ref_im, 0.0, None)
	flo_im = filters.normalize(flo_im, 0.0, None)
	
	diag = 0.5 * (transforms.image_diagonal(ref_im, spacing) + transforms.image_diagonal(flo_im, spacing))

	weights1 = np.ones(ref_im.shape)
	mask1 = np.ones(ref_im.shape, 'bool')
	weights2 = np.ones(flo_im.shape)
	mask2 = np.ones(flo_im.shape, 'bool')

	# Initialize registration framework for 2d images
	reg = Register(2)

	reg.set_report_freq(param_report_freq)
	reg.set_alpha_levels(alpha_levels)

	reg.set_reference_image(ref_im)
	reg.set_reference_mask(mask1)
	reg.set_reference_weights(weights1)

	reg.set_floating_image(flo_im)
	reg.set_floating_mask(mask2)
	reg.set_floating_weights(weights2)

	# Setup the Gaussian pyramid resolution levels
	if downScale:
		reg.add_pyramid_level(32, 5.0)
		reg.add_pyramid_level(16, 3.0)
		reg.add_pyramid_level(8, 0.0)
	else:
		reg.add_pyramid_level(8, 5.0)
		reg.add_pyramid_level(4, 3.0)
		reg.add_pyramid_level(2, 0.0)

	# Learning-rate / Step lengths [[start1, end1], [start2, end2] ...] (for each pyramid level)
	step_lengths = np.array([[1.0, 1.0], [1.0, 0.5], [0.5, 0.1]])

	# Create the transform and add it to the registration framework (switch between affine/rigid transforms by
	# commenting/uncommenting)
	# Affine
	reg.add_initial_transform(AffineTransform(2), np.array([1.0/diag, 1.0/diag, 1.0/diag, 1.0/diag, 1.0, 1.0]))
	# Rigid 2D
	# reg.add_initial_transform(Rigid2DTransform(2), np.array([1.0/diag, 1.0, 1.0]))

	# Set the parameters
	reg.set_iterations(param_iterations)
	reg.set_gradient_magnitude_threshold(0.001)
	reg.set_sampling_fraction(param_sampling_fraction)
	reg.set_step_lengths(step_lengths)

	print("Creating temporary directory")

	tmp_dir = os.path.join(output_path)

	# Create output directory
	if not os.path.exists(tmp_dir):
		os.makedirs(tmp_dir)

	# Start the pre-processing
	reg.initialize(tmp_dir+"/")
	
	# Control the formatting of numpy
	np.set_printoptions(suppress=True, linewidth=200)

	print("Registration...")

	# Start the registration
	reg.run()

	(transform, value) = reg.get_output(0)

	# Warp final image
	c = transforms.make_image_centered_transform(transform, ref_im, flo_im, spacing, spacing)

	# Print out transformation parameters
	print('Transformation parameters: %s.' % str(transform.get_params()))

	# Create the output image
	ref_im_warped = np.zeros(ref_im.shape)

	# Transform the floating image into the reference image space by applying transformation 'c'
	c.warp(In = flo_im_orig, Out = ref_im_warped, in_spacing=spacing, out_spacing=spacing, mode='spline', bg_value = 0.0)

	# imshowpair.imshowpair(ref_im_orig, ref_im_warped)
	# matplotlib.pyplot.show()

	
	# Save the registered image
	# outBaseDir = 'D:/Abel/SZBK/Projects/NatasaRegistration/JuhoImgs/registered/'
	# directory = os.path.dirname(outBaseDir + curr_img_string + "/")
	# if not os.path.exists(directory):
	# 	os.makedirs(directory)

	#print(ffilename)
	#print(rfilename)
	(ffilenameExex,_) = os.path.splitext(ffilename)
	io.imsave(output_path+'/'+ ffilenameExex + '_registered.tif', ref_im_warped)
	io.imsave(output_path+'/'+ rfilename, ref_im_orig)

	'''
	# Compute the absolute difference image between the reference and registered images
	D1 = np.abs(ref_im_orig-ref_im_warped)
	err = np.sum(D1)
	print("Err: %f" % err)

	io.imsave('./test_images/output/diff.png', D1)
	'''
	return transform

if __name__ == '__main__':
	start_time = time.time()
	if len(sys.argv)<5:
		ref_im_path = "D:/Abel/SZBK/Projects/Juho/PancreaticCancer/TMAs/testRun/TMA4B_DHCR7_PC27_21/TMA4B_DHCR7_PC27_21_08.tif"
		flo_im_path = "D:/Abel/SZBK/Projects/Juho/PancreaticCancer/TMAs/testRun/TMA4B_DHCR24_PC27_21/TMA4B_DHCR24_PC27_21_08.tif"
		tmpTgtDir = "D:/Abel/SZBK/Projects/Juho/PancreaticCancer/TMAs/testRun/results_preDeconv"
		outFilePath = "D:/Abel/SZBK/Projects/Juho/PancreaticCancer/TMAs/testRun/TMA4B_DHCR7_PC27_21/TMA4B_DHCR7_PC27_21_08.txt"
	else:
		ref_im_path = sys.argv[1]
		flo_im_path = sys.argv[2]
		tmpTgtDir = sys.argv[3]
		outFilePath = sys.argv[4]
	tform = main(ref_im_path, flo_im_path, tmpTgtDir, 0)
	
	smooth_transform_file = open(outFilePath, 'w')
	Tform = tform.get_params()
	for k in range(len(Tform)):
		smooth_transform_file.write(str(Tform[k]))
		smooth_transform_file.write(" ")
	smooth_transform_file.close()
	
	end_time = time.time()
	print("Elapsed time: " + str((end_time-start_time)))
