print "Hello world!\n"

import static qupath.lib.gui.scripting.QPEx.*
import qupath.lib.objects.PathObjects
import qupath.lib.roi.ROIs
import qupath.lib.regions.ImagePlane
import ij.*
import javax.imageio.ImageIO
import java.awt.Color
import qupath.lib.objects.classes.*
import qupath.lib.gui.images.servers.RenderedImageServer
import qupath.lib.gui.viewer.overlays.HierarchyOverlay
import qupath.lib.regions.RegionRequest

def thicken(annot, Integer width, h, v) {
//h,v boolean to indicate what sort of scaling to do.
    currROI = annot.getROI();
    List<PathObject> thickList = new ArrayList();
    upscaling = 2
    for (i = 1; i<=width; i++) {
        if (v) {
            for (j = -1; j<=1; j+=2) {
                tROI = currROI.translate(0,j*i*upscaling)
                tObject = PathObjects.createAnnotationObject(tROI, annot.getPathClass())
                addObject(tObject)
                thickList.add(tObject)
            }            
        }
        if (h) {
            for (j = -1; j<=1; j+=2) {
                tROI = currROI.translate(j*i*upscaling,0)
                tObject = PathObjects.createAnnotationObject(tROI, annot.getPathClass())
                addObject(tObject)
                thickList.add(tObject)
            }            
        }
        /*
        signum = ((i%2)-0.5)*2
        scale = 1 + signum * 0.01*Math.ceil(i/2)
        if (h) {sx = scale} else {sx = 1}
        if (v) {sy = scale} else {sy = 1}
        
        cx = currROI.getCentroidX()
        cy = currROI.getCentroidY()
        tROI = currROI.scale(sx,sy,cx,cy)
        tObject = PathObjects.createAnnotationObject(tROI, annot.getPathClass())
        addObject(tObject)
        thickList.add(tObject)
        */
    }
    return(thickList)
}

def exportROIsWithScaleBars(imageData, imgPath, thickWidth) {
    origImgName = ServerTools.getDisplayableImageName(imageData.getServer())
    if (containsClassStartingWith(imageData,"ROI")) {
        print("Exporting ROIs from " + origImgName + " width: " + thickWidth)
    } else {
        print("Skip.")
        return;
    }

    
    File sbFile = new File(buildFilePath(imgPath,origImgName + ".scaleBarSizes.csv")) //scale-bar file
    sbFile.write("ROIid, scaleBarSize [um]\n")
    
    hierarchy = imageData.getHierarchy()
    annotations = hierarchy.getAnnotationObjects()
    mrgn = 0.05 //margin percent
    int i = 1
    server = imageData.getServer()
    viewer = getCurrentViewer()
    
    // Define here a new pathClass
    int rgb = ((0&0x0ff)<<16)|((0&0x0ff)<<8)|(0&0x0ff); // see the RGB (( R &0x0ff)<<16)|(( G &0x0ff)<<8)|( B &0x0ff);
    sb = PathClassFactory.getPathClass("scaleBar")
    sb.setColor(rgb)
    
    /*
    Seems there is no need to add it specifically to the project PathClassifications
    pc = project.getPathClasses()
    print(pc)
    List<PathClass> pc2 = new ArrayList<PathClass>(pc);
    pc2.add(sb)
    pc2.remove(pc2.size()-1)
    print(pc2)
    //project.setPathClasses(pc2)
    
    // trials for thickening
    tl = thicken(annotations[1],4,true,true)
    
    Thread.sleep(5*1000)
    
    for (ta in tl) {
        removeObject(ta, Boolean.FALSE)
    }
    return;
    */
    
    thickList = new ArrayList();
    print("Thickening all ROI lines ("+ annotations.size() +")...")
    int c = 0
    for (annot in annotations) {
        thickList.addAll(thicken(annot,thickWidth,true,true))
        c++
        //print c + "/" + annotations.size() + " DONE."
    }
    
    print("Exporting the ROI class...")
    
    for (annot in annotations) {  
        if (annot.getPathClass().toString().startsWith("ROI")) {
            currROI = annot.getROI();
            w = currROI.getBoundsWidth()
            h = currROI.getBoundsHeight()
            ds = Math.ceil(Math.sqrt(w*h)/2048)
            request = RegionRequest.createInstance(server.getPath(), ds, currROI)
            imgName = origImgName + "." + annot.getPathClass().toString() + "." + i + ".w" + thickWidth
            f = new File(imgPath + "/" + imgName + ".png")
                   
            int z = 0
            int t = 0
            plane = ImagePlane.getPlane(z, t)
                    
            cal = server.getPixelCalibration()
            pixSize = cal.getAveragedPixelSizeMicrons()
            wum = w*pixSize // width of ROI in microns
            logOrig = Math.log10(wum/4)
            logInt = Math.floor(logOrig)
            logDecimal = logOrig-logInt
            scaleBarLengthUm = Math.pow(10,logInt)
            if (logDecimal>0.7) {
                scaleBarLengthUm = scaleBarLengthUm*5
            } else if (logDecimal>0.7) {
               scaleBarLengthUm = scaleBarLengthUm*2
            }
            sbl = scaleBarLengthUm/pixSize; //scaleBarLength in pixels
            sbFile << i + "," + scaleBarLengthUm + "\n"
            
            if (annot.getPathClass().toString() == "ROI_h-tl") {
                xs = currROI.getBoundsX() + w*mrgn;
                ys = currROI.getBoundsY() + h*mrgn;
                xe = xs+sbl;
                ye = ys;
            } else if (annot.getPathClass().toString() == "ROI_h-tr")  {        
                xs = currROI.getBoundsX() + w - w*mrgn - sbl;
                ys = currROI.getBoundsY() + h*mrgn;
                xe = xs+sbl;
                ye = ys;
            } else if (annot.getPathClass().toString() == "ROI_h-bl") {
                xs = currROI.getBoundsX() + w*mrgn;
                ys = currROI.getBoundsY() + h - h*mrgn;
                xe = xs+sbl;
                ye = ys;            
            } else if (annot.getPathClass().toString() == "ROI_v-br") {
                xs = currROI.getBoundsX() + w - w*mrgn;
                ys = currROI.getBoundsY() + h - h*mrgn - sbl;
                xe = xs;
                ye = ys + sbl;
            } else if (annot.getPathClass().toString() == "ROI_v-tl") {
                xs = currROI.getBoundsX() + w*mrgn;
                ys = currROI.getBoundsY() + h*mrgn;
                xe = xs;
                ye = ys + sbl;
            }
            
            roiSTR = annot.getPathClass().toString()
            directionLetter = roiSTR.charAt(roiSTR.size()-4)
                                            
            scaleBarROI = ROIs.createLineROI( xs, ys, xe, ye, plane)
            scaleBarAnnotation = PathObjects.createAnnotationObject(scaleBarROI, sb)
            addObject(scaleBarAnnotation)             
            
            // thicken also the scalebar
            if (directionLetter == 'h') {
                // if the line is horizontal then only vertically thicken it
                    scaleBarThickList = thicken(scaleBarAnnotation,thickWidth,false,true)
            } else if (directionLetter == 'v') {
                // if the line is vertical then only horizontally thicken it
                    scaleBarThickList = thicken(scaleBarAnnotation,thickWidth,true,false)
            }        
                            
            //writeRenderedImageRegion(viewer, request, f.toString());
	    def renderServer = new RenderedImageServer.Builder(imageData)
                .downsamples(ds)
                .layers(new HierarchyOverlay(viewer.getImageRegionStore(), viewer.getOverlayOptions(), imageData))
                .build()

            // Write or display the rendered image			           
            writeImageRegion(renderServer, request, f.toString())
            
            removeObject(scaleBarAnnotation, Boolean.FALSE)
            for (tsb in scaleBarThickList) {
                removeObject(tsb, Boolean.FALSE)
            }
            
            
            print "ROI " + i + " DONE."
            i++;               
        }
    }
    
    print("Removing thickening...")
    for (ta in thickList) {
        removeObject(ta, Boolean.FALSE)
    }
}

def containsClassStartingWith(imageData, strPattern) {
    res = false
    hierarchy = imageData.getHierarchy()
    annotations = hierarchy.getAnnotationObjects()
    for (annot in annotations) {  
        if (annot.getPathClass().toString().startsWith(strPattern)) {
            res = true
            break
        }
    }
    return(res)
}

project = getProject()

imgPath = buildFilePath("L:/ltdk_ikonen/Abel/Juho/PaCa/RevisionIHC/fullScreen/ROIs");
new File(imgPath).mkdir()

// Single img processing
imageData = getCurrentImageData()
//exportROIsWithScaleBars(imageData, imgPath, 4)
exportROIsWithScaleBars(imageData, imgPath, 16)

print "Image FINISHED!"
